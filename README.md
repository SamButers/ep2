![top](https://gitlab.com/SamButers/ep2/uploads/9e242f0cb2758b379dd4680e46250fb3/top.png)

[pokeball]: https://i.imgur.com/93b3Cl6.png
[superballmini]: https://i.imgur.com/46CHUKP.png

A Pokedex Java Application inspired by the Pokémon Black&White games.
## ![pokeball]	Summary
This Application has the following functionalities:

 - List all pokémons offered by the [PokéApi](https://pokeapi.co).
 - Create and save multiple trainers.
 - Create and show multiple trainers profiles.
 - Allow trainers to check the information regarding their pokémons, such as number of pokémons and each pokémon.
 - Search for pokémons by name and type.
 - Show varied informations regarding the pokémons.

This application use the **JavaFX** visual API, so it is **recommended** to you to download the latest [Java SE Development Kit 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) from the Oracle website, for your IDE installation may lack the necessary files to run a JavaFX application.

Since this Application gets resources from the [PokéApi](https://pokeapi.co), it needs a internet connection to work. Bad internet connections (such as public wireless or university wireless connections) **is known to cause exception throwing and perfomance decrease.** That being said, it is recommended to avoid those types of internet connections.

It is highly recommended for you to read this README, at least the Summary Section and the Possible bugs or problems section.
## ![pokeball]	How to use
### ![superballmini] Main Scene
Right after running the program, the following window will show up:<br/>
![mainscene1](https://gitlab.com/SamButers/ep2/uploads/9b9ebe1731264a03128aaacc3d52e2fe/mainscene1.png)<br/>
By hovering over each button, its respective images will show up at the top left as shown below:<br/>
![mainscene2](https://gitlab.com/SamButers/ep2/uploads/f531cdd8222422c9e7041f0dcf564f3d/mainscene2.png)<br/>
The Trainer button will send you to the Trainer Scene, the Search Button will send you to the Search Scene and the 'X' button, referred as exit button. Closing the application will save the Trainers' profiles.
### ![superballmini] Trainer Scene
By clicking on the Trainer Button, you will be brought up to the following window:<br/>
![trainerscene1](https://gitlab.com/SamButers/ep2/uploads/bbc56345386ff00ff275be76b236edf9/trainerscene1.png)<br/>
The Trainer Scene has five buttons, each serving as a slot for a trainer, and a blue arrow button, which will be referred as back, to send the user back to the Main Scene.
By clicking on one of the Create Trainer buttons, the following window will pop-up:<br/>
![trainerscene2](https://gitlab.com/SamButers/ep2/uploads/5cd279172a5d822859f4fbbeb400831f/trainerscene2.png)<br/>
Here the user will be able to create its trainer profile. It is important that the name has a max length of 13 characters and that a player avatar is selected, or the profile will not be created and the user will be met with a error sound.
After creating a profile the scene shall look like the following:  
![trainerscene3](https://gitlab.com/SamButers/ep2/uploads/1e04a269666566d4a2a06c22dc92d428/trainerscene3.png)<br/>
By hovering over the newly created Profile Button, the respective image with the Trainer's information will be show up as the following image:<br/>
![trainerscene4](https://gitlab.com/SamButers/ep2/uploads/c884d2467eea1c1b799ca172b89cc460/trainerscene4.png)<br/>
By clicking on the Profile Button, the user will be sent to the following window:<br/>
![trainerscene5](https://gitlab.com/SamButers/ep2/uploads/ea1c2e5df8bbf380043e5910a5dd1486/trainerscene5.png)<br/>
Here the user can see the information regarding the Selected Trainer's, such as the name, the number of pokémons, their ID Number and the list of pokémons. The ID Number will be a random generated number. It is important to remember that the last visited Trainer's Profile will be the **SELECTED** trainer.
In that case, the profile was just created and has no pokémons.
By clicking on any pokémon in the Trainer Profile Scene, the user will be brought to the Summary Scene.
### ![superballmini] Summary Scene
By clicking on a Pokémon Button in a Trainer's profile or in the Main Scene, the user will be sent to a Summary Scene that looks like the following:<br/>
![summaryscene1](https://gitlab.com/SamButers/ep2/uploads/eb1fe5c861ed2c87ecb775cbc3eafcce/summaryscene1.png)<br/>
The Summary Scene has three sub-scenes and a catch button.
The First Button will bring you to the Scene in the image on top and has basic information about the selected Pokémon.
A **Trainer's Information** will only appear if selected from a Trainer's Profile.
By clicking on the Second Button, the user will be brought to the following sub-scene:<br/>
![summaryscene2](https://gitlab.com/SamButers/ep2/uploads/cfaabd29a6866edaf1f8b24effa37340/summaryscene2.png)<br/>
This sub-scene contains the pokémon moves that are randomly selected upon creating and will not change until the Application restart. The moves will be maintained if it is caught by a Trainer and accessed from their profile.
By clicking on the Third Button (Info), the user will be brought to the following sub-scene:<br/>
![summaryscene3](https://gitlab.com/SamButers/ep2/uploads/e7ed33bb7bdb0529bef4423aa5dd5cc6/summaryscene3.png)<br/>
This sub-scene contains all the selected pokémon's stats as well as its description, if it exists.
By clicking on the Catch Button, a confirmation sound will play, **if a Trainer is selected**, else a error sound will play and nothing will happen.
By clicking on the Catch button and checking the profile we created as example it should look like this:<br/>
![summaryscene4](https://gitlab.com/SamButers/ep2/uploads/5e36ebaef6ec7d96824e5e094b013e63/summaryscene4.png)<br/>
You can go back and catch more pokémons, like this:<br/>
![summaryscene5](https://gitlab.com/SamButers/ep2/uploads/b5f87bc28673b4e0f76715859368fd98/summaryscene5.png)<br/>
You can catch as many ~~pikachus~~ pokémons as you would like. Enjoy it!
### ![superballmini] Search Scene
By clicking on the Search Button in the Main Scene, the user will be brought up to following window:<br/>
![searchscene1](https://gitlab.com/SamButers/ep2/uploads/152d750fc618fc0d9a70002db5dd361c/searchscene1.png)<br/>
Here the user can choose to search for a pokémon type or for a pokémon name. After typing your input, remember to press **ENTER** to search.
By inserting the word "pika" and searching for a type, it should look like this:<br/>
![searchscene2](https://gitlab.com/SamButers/ep2/uploads/c62fec5c0703685e2ee3d0ee5419bdec/searchscene2.png)<br/>
As an example, by selecting type and searching "ghost", it should look like this:<br/>
![searchscene3](https://gitlab.com/SamButers/ep2/uploads/20508ec40036191c0f8401ae8af6b5a7/searchscene3.png)<br/>
It is not recommended to search **"GHOST"** while having the **name** option selected. You've been warned.
## ![pokeball]	Possible bugs or problems
Known exceptions that are caused by bad internet connections are:

 - java.lang.IllegalArgumentException for image dimensions (observed to happen when trying to get image resources from the internet);
 - java.lang.reflect.InvocationTargetException;
 - java.net.UnknownHostException: pokeapi.co;
 - java.lang.RuntimeException: Exception in Application start method.

In a particular Ubuntu machine, it was observed a problem with image caching, although it doesn't seem to be a problem with the application, for it has not happened in other machines.

This application has been only tested using the **NetBeans IDE**, so if any compiling problem occurs, it is recommended trying using it.

The PokeAPI has been changed and some features might not work as intended.
## ![pokeball]	References
 - [JavaFX CSS Guide Reference - Oracle](https://docs.oracle.com/javafx/2/api/javafx/scene/doc-files/cssref.html);
 - [Java Documentation - Oracle](https://docs.oracle.com/en/java/);
 - [JavaFX Quick Guide - TutorialsPoint](https://www.tutorialspoint.com/javafx/javafx_quick_guide.htm);
 - [JavaFX Overview - Jenkov](http://tutorials.jenkov.com/javafx/overview.html);
 - [JavaFX Layout Panes - Zetcode](http://zetcode.com/gui/javafx/layoutpanes/).