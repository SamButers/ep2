package control;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import java.lang.Math;


public class JSONCapturer {
    
        private URL target;
        private URL previousTarget;
        private JSONObject previousObject;
        
        
        public JSONCapturer(URL target) throws MalformedURLException{
            setTarget(target);
        }
        
        
        public URL getTarget() {
            return target;
        }

        public void setTarget(URL target) {
            this.target = target;
        }
      
        public String returnParameter(String parameter, int id) throws IOException, ParseException{
            String parameterLabel;
            StringBuffer URLString = new StringBuffer();
            String getLine;
            
            if(!parameter.equals("flavor_text")){                
                URLString.append(this.target.toString());
                URLString.append("pokemon/");
                URLString.append(id);
                URLString.append('/');
            }
            
            else{
                URLString.append(this.target.toString());
                URLString.append("pokemon-species/");
                URLString.append(id);
                URLString.append('/');                
            }
            
            URL zelda = new URL(URLString.toString());
            
            HttpURLConnection connection;
            int responseCode;
            
            if(!zelda.equals(this.previousTarget)){
                connection = (HttpURLConnection) zelda.openConnection();
            
                connection.addRequestProperty("User-Agent", "Mozilla/4.76");
                connection.setRequestMethod("GET");
            
                responseCode = connection.getResponseCode();
            }
            
            else{
                responseCode = HttpURLConnection.HTTP_OK;
                connection = null;
            }
            
            if(responseCode == HttpURLConnection.HTTP_OK){
                JSONObject currentPokemon = new JSONObject();
                
                if(!zelda.equals(this.previousTarget)){
                    BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                
                    StringBuffer responseString = new StringBuffer();
                
                    while((getLine = input.readLine()) != null){
                        responseString.append(getLine);
                    }
                
                    input.close();
                
                    JSONParser jasonParser = new JSONParser();
                
                    JSONObject jasonOBJ = (JSONObject)jasonParser.parse(responseString.toString());
                    currentPokemon = (JSONObject)jasonOBJ;
                    
                    this.previousTarget = zelda;
                    this.previousObject = currentPokemon;
                }
                
                else{
                    currentPokemon = this.previousObject;
                }

                JSONArray jasonArray = new JSONArray(); // -- May have problems by iniatilizing here. We shall see.
                
                if(parameter.equals("name")){
                    currentPokemon = (JSONObject) currentPokemon.get("species");
                }
                
                if(parameter.equals("type")){
                    jasonArray = (JSONArray) currentPokemon.get("types");
                        currentPokemon = (JSONObject) jasonArray.get(0);
                        currentPokemon = (JSONObject) currentPokemon.get("type");
                        
                        parameterLabel = new String((currentPokemon.get("name")).toString());
                    
                        return parameterLabel;
                }
                
                if(parameter.equals("type2")){
                    jasonArray = (JSONArray) currentPokemon.get("types");
                        if(jasonArray.size() == 2){
                            currentPokemon = (JSONObject) jasonArray.get(1);
                            currentPokemon = (JSONObject) currentPokemon.get("type");
                        
                            parameterLabel = new String((currentPokemon.get("name")).toString());
                    
                            return parameterLabel;
                        }
                        
                        else{
                            return "null";
                        }
                }
                
                if(parameter.equals("attack")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(4);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("defense")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(3);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("special-attack")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(2);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("special-defense")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(1);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("speed")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(0);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("hp")){
                    jasonArray = (JSONArray) currentPokemon.get("stats");
                    currentPokemon = (JSONObject) jasonArray.get(0);
                    
                    int assistInt = Math.toIntExact((long) currentPokemon.get("base_stat"));
                    
                    parameterLabel = new String(Integer.toString(assistInt));
                    return parameterLabel;                    
                }
                
                if(parameter.equals("flavor_text")){
                    jasonArray = (JSONArray) currentPokemon.get("flavor_text_entries");
                    JSONObject language;
                    for(int c = 0; c < jasonArray.size(); c++){
                        language = (JSONObject) jasonArray.get(c);
                        language = (JSONObject) language.get("language");
                        if(language.get("name").toString().equals("en")){
                            currentPokemon = (JSONObject) jasonArray.get(c);
                            break;
                        }
                    }
                }
                
                if(parameter.equals("move")){
                    jasonArray = (JSONArray) currentPokemon.get("moves");
                    
                    Random randomNumber = new Random();
                    currentPokemon = (JSONObject) jasonArray.get(randomNumber.nextInt(jasonArray.size()));
                    currentPokemon = (JSONObject) currentPokemon.get("move");
                    
                    parameterLabel = new String(currentPokemon.get("name").toString());
                    return parameterLabel;
                }
                
                if(parameter.equals("movesize")){
                    jasonArray = (JSONArray) currentPokemon.get("moves");
                    
                    parameterLabel = new String(Integer.toString(jasonArray.size()));
                    return parameterLabel;
                }
                
                parameterLabel = new String((currentPokemon.get(parameter)).toString());
            }
            
            else{
                parameterLabel = new String("72");
            }
            
            return parameterLabel;
        }

        public JSONObject returnJSON() throws MalformedURLException, IOException, ParseException{
            JSONObject json;
            String getLine;
            
            URL zelda  = new URL(this.target.toString() + "pokemon/?offset=0&limit=964");
            
            HttpURLConnection connection = (HttpURLConnection) zelda.openConnection();
            
            connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            connection.setRequestMethod("GET");
            
            int response = connection.getResponseCode();
            
            if(response == HttpURLConnection.HTTP_OK){
                BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                
                StringBuffer responseString = new StringBuffer();
                
                while((getLine = input.readLine()) != null){
                    responseString.append(getLine);
                }
                input.close();
                
                JSONParser jasonParser = new JSONParser();
                
                json = (JSONObject) jasonParser.parse(responseString.toString());
                
            }
            
            else{
                json = null;
            }
            
            return json;
        }

        public JSONArray returnTypeArray(int type) throws MalformedURLException, IOException, ParseException {
            JSONObject json;
            JSONArray array;
            String getLine;
            
            URL zelda  = new URL(this.target.toString() + "type/" + type + "/");
            
            HttpURLConnection connection = (HttpURLConnection) zelda.openConnection();
            
            connection.addRequestProperty("User-Agent", "Mozilla/4.76");
            connection.setRequestMethod("GET");
            
            int response = connection.getResponseCode();
            
            if(response == HttpURLConnection.HTTP_OK){
                BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                
                StringBuffer responseString = new StringBuffer();
                
                while((getLine = input.readLine()) != null){
                    responseString.append(getLine);
                }
                input.close();
                
                JSONParser jasonParser = new JSONParser();
                
                json = (JSONObject) jasonParser.parse(responseString.toString());
                
            }
            
            else{
                json = null;
            }
            
            if(json == null){
                return null;
            }
            
            array = (JSONArray) json.get("pokemon");
            return array;
        }
}
