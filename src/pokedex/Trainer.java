package pokedex;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import javafx.scene.image.Image;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import pokedex.Pokemon;

public class Trainer {
    private String name, imageTarget;
    private Image image;
    private int pokemonNumber, id;
    private ArrayList<Pokemon> pokemonList = new ArrayList<Pokemon>();

    public Trainer(String name, String imageTarget, int pokemonNumber, ArrayList<Pokemon> pokemonList){
        setName(name);
        setImageTarget(imageTarget);
        setImage(imageTarget);
        setPokemonNumber(pokemonNumber);
        setPokemonList(pokemonList);
        setId();
    }
    
    public Trainer(String name, String imageTarget, int pokemonNumber){
        setName(name);
        setImageTarget(imageTarget);
        setImage(imageTarget);
        setPokemonNumber(pokemonNumber);
        setId();
    }
    
    public Trainer(JSONObject trainerJson){
        setName(trainerJson.get("name").toString());
        setImageTarget(trainerJson.get("image").toString());
        setImage(trainerJson.get("image").toString());
        Long counter = (long) trainerJson.get("pokemonCounter");
        setPokemonNumber(counter.intValue());
        Long id = (long) trainerJson.get("id");
        setId(id.intValue());
        
        ArrayList<Pokemon> pokemonList = new ArrayList<Pokemon>();
        
        JSONArray pokemonArray = (JSONArray) trainerJson.get("pokemons");
        for(int c = 0; c < pokemonArray.size(); c++){
            JSONObject pokemonObj = (JSONObject) pokemonArray.get(c);
            
            JSONArray moveArray = (JSONArray) pokemonObj.get("moves");
            ArrayList<String> moves = new ArrayList<String>();
            
            for(int x = 0; x < moveArray.size(); x++){
                JSONObject move = (JSONObject) moveArray.get(x);
                moves.add(move.get("name").toString());
            }
            
            Pokemon pokemon;
            
            pokemon = new Pokemon(pokemonObj.get("name").toString(), pokemonObj.get("owner").toString(), pokemonObj.get("type1").toString(), pokemonObj.get("type2").toString(), (int) (long) pokemonObj.get("id"), (int) (long) pokemonObj.get("hp"), (int) (long) pokemonObj.get("attack"), (int) (long) pokemonObj.get("defense"), (int) (long) pokemonObj.get("spAtk"), (int) (long) pokemonObj.get("spDef"), (int) (long) pokemonObj.get("speed"), (int) (long) pokemonObj.get("height"), (int) (long) pokemonObj.get("weight"), pokemonObj.get("description").toString(), new Image(pokemonObj.get("image").toString()), moves);
            
            if(pokemon.getImage().isError()){
                pokemon.setImage(new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString()));
            }
            
            pokemonList.add(pokemon);
        }
        
        setPokemonList(pokemonList);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(){
        Random random = new Random();
        this.id = random.nextInt(99999);
    }
    
    public void setId(int id){
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    
    public void setImage(String imageTarget){
        if(imageTarget.equals("red")){
            this.image = new Image(new File("src/resources/tiles/red.png").toURI().toString());
        }
        
        if(imageTarget.equals("hilbert")){
            this.image = new Image(new File("src/resources/tiles/hilbert.png").toURI().toString());
        }
        
        if(imageTarget.equals("elio")){
            this.image = new Image(new File("src/resources/tiles/elio.png").toURI().toString());
        }
        
        if(imageTarget.equals("leaf")){
            this.image = new Image(new File("src/resources/tiles/leaf.png").toURI().toString());
        }
        
        if(imageTarget.equals("hilda")){
            this.image = new Image(new File("src/resources/tiles/hilda.png").toURI().toString());
        }
        
        if(imageTarget.equals("moon")){
            this.image = new Image(new File("src/resources/tiles/moon.png").toURI().toString());
        }
    }

    public int getPokemonNumber() {
        return pokemonNumber;
    }

    public void setPokemonNumber(int pokemonNumber) {
        this.pokemonNumber = pokemonNumber;
    }

    public ArrayList<Pokemon> getPokemonList() {
        return pokemonList;
    }

    public void setPokemonList(ArrayList<Pokemon> pokemonList) {
        this.pokemonList = pokemonList;
    }
    
    public JSONObject toJson(){
        JSONObject trainerJson = new JSONObject();
        
        trainerJson.put("name", getName());
        trainerJson.put("image", getImageTarget());
        trainerJson.put("pokemonCounter", getPokemonNumber());
        trainerJson.put("id", getId());

        JSONArray pokemonArray = new JSONArray();
        JSONObject pokemonObject = new JSONObject();
        for(int c = 0; c < getPokemonList().size(); c++){
            pokemonObject = getPokemonList().get(c).toJson();
            pokemonArray.add(pokemonObject);
        }
        
        trainerJson.put("pokemons", pokemonArray);
        return trainerJson;
    }

    public String getImageTarget() {
        return imageTarget;
    }

    public void setImageTarget(String imageTarget) {
        this.imageTarget = imageTarget;
    }
    
}
