package pokedex;

import java.util.ArrayList;
import javafx.scene.image.Image;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Pokemon{
	private String name, owner, typeName, typeName2, description;
	private int number, hp, attack, defense, spAtk, spDef, speed, height, weight;
        private Image image;
        private ArrayList<String> moves;
//	private Type type; --May not be used, but will remain here just to make sure.
	
	public Pokemon(String name, String owner, String typeName, String typeName2, int number, int hp, int attack, int defense, int spAtk, int spDef, int speed, int height, int weight, String description, Image image, ArrayList<String> moves){
		setName(name);
                setOwner(owner);
                setTypeName(typeName);
                setTypeName2(typeName2);
                setNumber(number);
                setHp(hp);
                setAttack(attack);
                setDefense(defense);
                setSpAtk(spAtk);
                setSpDef(spDef);
                setSpeed(speed);
                setHeight(height);
                setWeight(weight);
                setDescription(description);
                setImage(image);
                setMoves(moves);
	}
	
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public String getOwner(){
		return owner;
	}
	
	public void setOwner(String owner){
		this.owner = owner;
	}
	
	public int getHp(){
		return hp;
	}
	
	public void setHp(int hp){
		this.hp = hp;
	}
	
	public int getAttack(){
		return attack;
	}
	
	public void setAttack(int attack){
		this.attack = attack;
	}
	
	public int getDefense(){
		return defense;
	}
	
	public void setDefense(int defense){
		this.defense = defense;
	}
	
	public int getSpAtk(){
		return spAtk;
	}
	
	public void setSpAtk(int spAtk){
		this.spAtk = spAtk;
	}
	
	public int getSpDef(){
		return spDef;
	}
	
	public void setSpDef(int spDef){
		this.spDef = spDef;
	}
	
	public int getSpeed(){
		return speed;
	}
	
	public void setSpeed(int speed){
		this.speed = speed;
	}
	
	public int getNumber(){
		return number;
	}
	
	public void setNumber(int number){
		this.number = number;
	}
	
/*	private void setTypeVar() {
		if(getTypeName() == "Water")
			type = new Water();
		
		if(getTypeName() == "Fire")
			type = new Fire();
		
		if(getTypeName() == "Grass")
			type = new Grass();
		
		if(getTypeName() == "Bug")
			type = new Bug();
	}*/ //May not be used, but will remain here just to make sure.

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description.equals("72")){
            this.description = "No description found.\nThis pokemon either has no description or is a version of\nanother pokémon.\nIf your case is the latter, check for the original pokemon.";
        }
        else{
            this.description = description;
        }
    }

    public String getTypeName2() {
        return typeName2;
    }

    public void setTypeName2(String typeName2) {
        this.typeName2 = typeName2;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ArrayList<String> getMoves() {
        return moves;
    }

    public void setMoves(ArrayList<String> moves) {
        this.moves = moves;
    }
    
    public JSONObject toJson() {
        JSONObject pokeJson = new JSONObject();
        
        pokeJson.put("name", getName());
        pokeJson.put("owner", getOwner());
        pokeJson.put("type1", getTypeName());
        pokeJson.put("type2", getTypeName2());
        pokeJson.put("description", getDescription());
        pokeJson.put("id", getNumber());
        pokeJson.put("hp", getHp());
        pokeJson.put("attack", getAttack());
        pokeJson.put("defense", getDefense());
        pokeJson.put("spAtk", getSpAtk());
        pokeJson.put("spDef", getSpDef());
        pokeJson.put("speed", getSpeed());
        pokeJson.put("height", getHeight());
        pokeJson.put("weight", getWeight());
        pokeJson.put("image", "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + getNumber() + ".png");
        
        JSONArray pokeArray = new JSONArray();
        JSONObject tempObj;
        for(int c = 0; c < getMoves().size(); c++){
            tempObj = new JSONObject();
            tempObj.put("name", getMoves().get(c));
            
            pokeArray.add(tempObj);
        }
        
        pokeJson.put("moves", pokeArray);
        
        return pokeJson;
    }
}
