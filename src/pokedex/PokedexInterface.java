package pokedex;

import java.util.ArrayList;
import control.JSONCapturer;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.stage.*;
import javafx.scene.*;
import javafx.application.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import main.pokedexAction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PokedexInterface extends Application {

    private ArrayList<HollowPokemon> pokemonHollowList = new ArrayList<HollowPokemon>();
    private ArrayList<Pokemon> pokemonList = new ArrayList<Pokemon>();
    private ArrayList<Trainer> trainerArray = new ArrayList<Trainer>();
    private ArrayList<JSONArray> typesArray = new ArrayList<JSONArray>();
    private ArrayList<Integer> caughtTypes = new ArrayList<Integer>();
    private double xOffset = 0;
    private double yOffset = 0;
    private Stage pokeWindow = new Stage();
    private volatile boolean sceneReady = false;
    private volatile boolean justStarted = true;
    private volatile boolean profileIsOpen = false;
    private volatile String targetScene;
    private String targettedTrainer;
    private volatile Scene currentScene;
    private volatile Scene mainScene;
    private volatile int globalId, globalPos, targettedButton, targettedPokemon;
    private volatile int currentTrainer = -1;
    Runnable sceneRunnable = new Runnable(){
        @Override
        public void run() {
            sceneReady = false;
            if(targetScene.equals("summaryScene")){
                try {
                    setSummaryScene();
                } catch (Exception ex) {
                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                sceneReady = true;
            }
        }
    };
    
    private void loadScene(){
        Thread setScene = new Thread(sceneRunnable);
        setScene.start();
        
        if(!sceneReady){
            GridPane loadPane = new GridPane();
            
            ImageView topPart = new ImageView(new Image(new File("src/resources/tiles/loadingbackground2.png").toURI().toString()));
            ImageView bottomPart = new ImageView(new Image(new File("src/resources/tiles/loadingbackground1.png").toURI().toString()));
            
            TranslateTransition topAnimation = new TranslateTransition(Duration.millis(700), topPart);
            TranslateTransition bottomAnimation = new TranslateTransition(Duration.millis(700), bottomPart);
            
            topAnimation.setFromY(-416);
            topAnimation.setToY(0);
            bottomAnimation.setFromY(416);
            bottomAnimation.setToY(0);
            
            ParallelTransition syncTransitions = new ParallelTransition(topAnimation, bottomAnimation);
            
            syncTransitions.setOnFinished(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent event) {
                    while(!sceneReady){}
                    pokeWindow.setScene(currentScene);
                    sceneReady = false;
                }
            });
            
            loadPane.add(topPart, 0, 0);
            loadPane.add(bottomPart, 0, 0);
            
            this.pokeWindow.setScene(new Scene(loadPane, 512, 416));
            
            syncTransitions.play();
        }
        
    }
    
    private void initializePokeArray() throws MalformedURLException, IOException, ParseException{
        JSONCapturer jasonCapturer = new JSONCapturer(new URL("https://pokeapi.co/api/v2/"));
        JSONObject pokeList = jasonCapturer.returnJSON();
        JSONArray pokeArrayList = (JSONArray) pokeList.get("results");
        
        if(!pokeList.equals(null)){
            for(int c = 0; c < 949; c++){
                JSONObject currentPokemon = (JSONObject) pokeArrayList.get(c);
                
                this.pokemonHollowList.add(new HollowPokemon(currentPokemon.get("name").toString(), c, new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString())));
            }
        }
        
        else{
            System.out.println("PokeArray connection failed.");
        }
    }
    
    private int searchPokeArray(int id){
        for(int c = 0; c < this.pokemonList.size(); c++){
            if(this.pokemonList.get(c).getNumber() == id){
                return c;
            }
        }
        
        return (-1);
    }
    
    private BufferedImage cropImage(BufferedImage image){
        WritableRaster pixelReader = image.getAlphaRaster();
        
        int width = pixelReader.getWidth();
        int height = pixelReader.getHeight();
        int left = 0;
        int top = 0;
        int right = width - 1;
        int bottom = height - 1;
        
        int mBottom = bottom;
        int mTop = top;
        int mLeft = width;
        int mRight = height;
        
        top:
        for(; top < bottom; top++){
            for(int x = 0; x < width; x++){
                if(pixelReader.getSample(x, top, 0) != 0){
                    mTop = top;
                    break top;
                }
            }
        }
        
        bottom:
        for(; bottom > mTop; bottom--){
            for(int x = 0; x < width; x++){
                if(pixelReader.getSample(x, bottom, 0) != 0){
                    mBottom = bottom;
                    break bottom;
                }
            }
        }
        
        left:
        for(; left < right; left++){
            for(int y = mTop; y < mBottom; y++){
                if(pixelReader.getSample(left, y, 0) != 0){
                    mLeft = left;
                    break left;
                }
            }
        }
        
        right:
        for(; right > mLeft; right--){
            for(int y = mTop; y < mBottom; y++){
                if(pixelReader.getSample(right, y, 0) != 0){
                    mRight = right;
                    break right;
                }
            }
        }
        
        return image.getSubimage(mLeft, mTop, mRight - mLeft + 1, mBottom - mTop + 1);
    }
    
    private void readTrainersFile() throws FileNotFoundException, IOException, ParseException {
        File trainersConfig = new File("src/trainers.data");
                
        BufferedReader getLine = new BufferedReader(new FileReader(trainersConfig));
        StringBuffer jsonLine = new StringBuffer();
        String line;
        
        while((line = getLine.readLine()) != null){
            jsonLine.append(line);
        }
        
        getLine.close();
        
        if((jsonLine.toString().length() == 0) || (jsonLine.toString().length() == 2)){
            return;
        }
        
        JSONParser parser = new JSONParser();
        JSONArray trainerJson = (JSONArray) parser.parse(jsonLine.toString());
        
        for(int c = 0; c < trainerJson.size(); c++){
            JSONObject currentTrainer = (JSONObject) trainerJson.get(c);
            
            this.trainerArray.add(new Trainer(currentTrainer));
        }
    }
    
    private void saveTrainers() throws IOException{
        File trainersConfig = new File("src/trainers.data");
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(trainersConfig));
        JSONArray trainersJson = new JSONArray();
        
        for(int c = 0; c < this.trainerArray.size(); c++){
            trainersJson.add(this.trainerArray.get(c).toJson());
        }
        
        writer.write(trainersJson.toJSONString());
        
        writer.close();
    }
    
    private void setMainScene() throws Exception{
        GridPane rootNode = new GridPane();
        
        rootNode.setGridLinesVisible(false);
        
        ColumnConstraints columnConst1 = new ColumnConstraints();
        ColumnConstraints columnConst2 = new ColumnConstraints();
        
        columnConst1.setMinWidth(243);
        columnConst1.setHgrow(Priority.ALWAYS);
        
        columnConst2.setMinWidth(269);
        columnConst2.setHgrow(Priority.ALWAYS);
        
        RowConstraints rowConst1 = new RowConstraints();
        RowConstraints rowConst2 = new RowConstraints();
        RowConstraints rowConst3 = new RowConstraints();
        
        rowConst1.setMinHeight(30);
        rowConst1.setVgrow(Priority.ALWAYS);
        
        rowConst2.setMinHeight(339);
        rowConst2.setVgrow(Priority.NEVER);
        
        rowConst3.setMinHeight(47);
        rowConst3.setVgrow(Priority.NEVER);

        
        rootNode.getColumnConstraints().addAll(columnConst1, columnConst2);
        rootNode.getRowConstraints().addAll(rowConst1, rowConst2, rowConst3);

        
        ImageView pokeImageView = new ImageView(new Image(new File("src/resources/tiles/transparent.png").toURI().toString()));
        
        GridPane imageGrid = new GridPane();
        
        RowConstraints imageConst = new RowConstraints();
        imageConst.setMinHeight(40);
        imageConst.setVgrow(Priority.NEVER);
        RowConstraints imageConst2 = new RowConstraints();
        imageConst2.setMinHeight(128);
        imageConst.setVgrow(Priority.NEVER);
        imageGrid.getRowConstraints().addAll(imageConst, imageConst2);
        
        ColumnConstraints imageConst3 = new ColumnConstraints();
        imageConst3.setMinWidth(18);
        imageConst3.setHgrow(Priority.NEVER);
        imageGrid.getColumnConstraints().add(imageConst3);
        imageGrid.add(pokeImageView, 1, 0);
        
        pokeImageView.setPreserveRatio(true);
        imageGrid.setGridLinesVisible(false);

        
        ScrollPane listScroll = new ScrollPane();
        
        VBox pokeButtons = new VBox();
        ArrayList<Button> buttonArray = new ArrayList<Button>();

        initializePokeArray();
        for(int c = 0; c < 949; c++){
            String capitalized = (this.pokemonHollowList.get(c).getName()).substring(0, 1).toUpperCase() + this.pokemonHollowList.get(c).getName().substring(1);
            StringBuffer trueId = new StringBuffer();
            final int counterC = c;
            
            Button buttonC = new Button(capitalized);
            
            buttonC.setId("pokemonbuttons");
            buttonC.setMinWidth(238);
            buttonC.setPrefWidth(238);
            buttonC.setMinHeight(42);
            buttonC.setPrefHeight(42);
  
            buttonC.setOnMouseEntered(new EventHandler<MouseEvent>(){
                public void handle(MouseEvent e){
                    String uri = new String("src/resources/sounds/SelectionMove.wav");
                    Media sound = new Media(new File(uri).toURI().toString());
                    MediaPlayer soundSelectPlay = new MediaPlayer(sound);
                    
                    soundSelectPlay.play();
                    
                    if(counterC < 802){
                        pokemonHollowList.get(counterC).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (counterC + 1) + ".png"));
                    }
                    
                    if((counterC > 802)&&(counterC < 892)){
                        pokemonHollowList.get(counterC).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (counterC + 9199) + ".png"));
                    }
                    
                    if(counterC > 892){
                        pokemonHollowList.get(counterC).setPokeImage(new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString()));
                    }
                    
                //PixelReader resizing sample from jewelsea on Github gist
                        int width = (int) pokemonHollowList.get(counterC).getPokeImage().getWidth();
                        int height = (int) pokemonHollowList.get(counterC).getPokeImage().getHeight();
    
                        WritableImage output = new WritableImage(width * 2, height * 2);
    
                        PixelReader reader = pokemonHollowList.get(counterC).getPokeImage().getPixelReader();
                        PixelWriter writer = output.getPixelWriter();

                        for (int y = 0; y < height; y++) {
                            for (int x = 0; x < width; x++) {
                                final int argb = reader.getArgb(x, y);
                                for (int dy = 0; dy < 2; dy++) {
                                    for (int dx = 0; dx < 2; dx++) {
                                        writer.setArgb(x * 2 + dx, y * 2 + dy, argb);
                                    }
                                }
                            }
                        }
                //PixelReader resizing sample from jewelsea on Github gist
                    
                        pokeImageView.setImage(output);
                        pokeImageView.setFitHeight(200);
                    
                }
            });
            
            buttonC.setOnMouseClicked(new EventHandler<MouseEvent>(){
                public void handle(MouseEvent e){
                    try {
                        String uri = new String("src/resources/sounds/Confirm.wav");
                        Media sound = new Media(new File(uri).toURI().toString());
                        MediaPlayer soundConfirmPlay = new MediaPlayer(sound);
                    
                        soundConfirmPlay.play();
                        //Change the scene here. Maybe send a pokemon.
                        globalId = pokemonHollowList.get(counterC).getId();
                        globalPos = counterC;
                        targetScene = "summaryScene";
                        targettedPokemon = -1;
                        loadScene();
                    } catch (Exception ex) {
                        Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            buttonArray.add(buttonC);
        }
        
        
        rootNode.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                xOffset = e.getSceneX();
                yOffset = e.getSceneY();
            }
        });
        
        rootNode.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                pokeWindow.setX(e.getScreenX() - xOffset);
                pokeWindow.setY(e.getScreenY() - yOffset);
            }
        });

        
        pokeButtons.getChildren().addAll(buttonArray);
        pokeButtons.setSpacing(10);
        
        listScroll.setContent(pokeButtons);
        listScroll.fitToHeightProperty();
        listScroll.fitToWidthProperty();
        listScroll.setBackground(Background.EMPTY);

        rootNode.add(listScroll, 1, 1);
        rootNode.add(imageGrid, 0, 1);
        rootNode.setFillWidth(listScroll, true);
        rootNode.setFillHeight(listScroll, true);
        
        MediaPlayer confirm = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm.wav").toURI().toString())));
        
        Button trainerButton = new Button();
        trainerButton.setMinSize(68, 40);
        trainerButton.setPrefSize(68, 40);
        trainerButton.setMaxSize(68, 40);
        trainerButton.setId("trainerbutton");
        trainerButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                try {
                    confirm.play();
                    setTrainerScene();
                } catch (Exception ex) {
                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                    confirm.seek(Duration.ZERO);
            }
        });
        
        Button searchButton = new Button();
        searchButton.setMinSize(68, 40);
        searchButton.setPrefSize(68, 40);
        searchButton.setMaxSize(68, 40);
        searchButton.setId("searchbutton");
        searchButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                confirm.play();
                try {
                    setSearchScene();
                } catch (Exception ex) {
                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                confirm.seek(Duration.ZERO);
            }
        });
        
        Button exit = new Button();
        exit.setMinWidth(28);
        exit.setPrefWidth(28);
        exit.setMinHeight(28);
        exit.setPrefHeight(28);
        exit.setId("exit");
        
        MediaPlayer backSound = new MediaPlayer(new Media((new File("src/resources/sounds/Back.wav").toURI().toString())));

        exit.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                backSound.play();
                backSound.seek(Duration.ZERO);
                try {
                    saveTrainers();
                } catch (IOException ex) {
                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.exit();
            }
        });
        
        GridPane divisory = new GridPane();
        
        ColumnConstraints division1 = new ColumnConstraints();
        ColumnConstraints division2 = new ColumnConstraints();
        
        division1.setMinWidth(256);
        division1.setPrefWidth(256);
        division1.setMaxWidth(256);
        division2.setMinWidth(256);
        division2.setPrefWidth(256);
        division2.setMaxWidth(256);
        
        divisory.getColumnConstraints().addAll(division1, division2);
        
        GridPane exitPane = new GridPane();
        GridPane trainerSearchPane = new GridPane();
        
        trainerSearchPane.add(trainerButton, 0, 0);
        trainerSearchPane.add(searchButton, 0, 0);
        trainerSearchPane.setAlignment(Pos.CENTER_LEFT);
        exitPane.add(exit, 0, 0);
        exitPane.setAlignment(Pos.CENTER_RIGHT);
        
        exit.setTranslateX(-15);
        trainerButton.setTranslateX(15);
        searchButton.setTranslateX(95);

        divisory.add(trainerSearchPane, 0, 0);
        divisory.add(exitPane, 1, 0);
        
        rootNode.add(divisory, 0, 2);
                
        Scene pokedex = new Scene(rootNode, 512, 384);
        pokedex.getStylesheets().add(getClass().getResource("paneStyle.css").toExternalForm());
        
        this.pokeWindow.setScene(pokedex);
        this.mainScene = pokedex;
        
        this.pokeWindow.show();
    }
    
    private void setSummaryScene() throws Exception{
        GridPane summaryPane = new GridPane();
        summaryPane.setGridLinesVisible(false);
        
        ColumnConstraints column1 = new ColumnConstraints();
        ColumnConstraints column2 = new ColumnConstraints();
        ColumnConstraints column3 = new ColumnConstraints();
        RowConstraints row1 = new RowConstraints();
        RowConstraints row2 = new RowConstraints();
        RowConstraints row3 = new RowConstraints();
        RowConstraints row4 = new RowConstraints();
        
        column1.setMinWidth(158);
        column1.setHgrow(Priority.NEVER);
        column2.setMinWidth(156);
        column2.setHgrow(Priority.NEVER);
        column3.setMinWidth(198);
        column3.setHgrow(Priority.NEVER);
        
        row1.setMinHeight(41);
        row1.setVgrow(Priority.NEVER);
        row2.setMinHeight(169);
        row2.setVgrow(Priority.NEVER);
        row3.setMinHeight(158);
        row3.setVgrow(Priority.NEVER);
        row4.setMinHeight(48);
        row4.setVgrow(Priority.NEVER);
        
        summaryPane.getColumnConstraints().addAll(column1, column2, column3);
        summaryPane.getRowConstraints().addAll(row1, row2, row3, row4);
        
        GridPane labelGrid = new GridPane();
        GridPane labelValueGrid = new GridPane();
        
        RowConstraints labelRow1 = new RowConstraints();
        RowConstraints labelRow2 = new RowConstraints();
        RowConstraints labelRow3 = new RowConstraints();
        RowConstraints labelRow4 = new RowConstraints();
        RowConstraints labelRow5 = new RowConstraints();
        
        labelRow1.setMinHeight(34);
        labelRow2.setMinHeight(34);
        labelRow3.setMinHeight(34);
        labelRow4.setMinHeight(34);
        labelRow5.setMinHeight(33);
        labelRow1.setVgrow(Priority.NEVER);
        labelRow2.setVgrow(Priority.NEVER);
        labelRow3.setVgrow(Priority.NEVER);
        labelRow4.setVgrow(Priority.NEVER);
        labelRow5.setVgrow(Priority.NEVER);
        
        labelGrid.getRowConstraints().addAll(labelRow1, labelRow2, labelRow3, labelRow4, labelRow5);
        labelValueGrid.getRowConstraints().addAll(labelRow1, labelRow2, labelRow3, labelRow4, labelRow5);

        Label dexNo = new Label("   ▪ Dex No.");
        Label pokeName = new Label("   ▪ Name");
        Label pokeTypes = new Label("   ▪ Type");
        Label pokeOwner = new Label("   ▪ Owner");
        Label ownerID = new Label("   ▪ ID no.");
        
        dexNo.setId("infolabel");
        pokeName.setId("infolabel");
        pokeTypes.setId("infolabel");
        pokeOwner.setId("infolabel");
        ownerID.setId("infolabel");

        labelGrid.add(dexNo, 0, 0);
        labelGrid.add(pokeName, 0, 1);
        labelGrid.add(pokeTypes, 0, 2);
        labelGrid.add(pokeOwner, 0, 3);
        labelGrid.add(ownerID, 0, 4);
        
        GridPane buttonGrid = new GridPane();
        
        ColumnConstraints buttonColumn1 = new ColumnConstraints();
        ColumnConstraints buttonColumn2 = new ColumnConstraints();
        ColumnConstraints buttonColumn3 = new ColumnConstraints();
        RowConstraints buttonRow = new RowConstraints();
        
        buttonColumn1.setMinWidth(7);
        buttonColumn2.setMinWidth(77);
        buttonColumn3.setMinWidth(232);
        buttonRow.setMinHeight(5);
        buttonColumn1.setHgrow(Priority.NEVER);
        buttonColumn2.setHgrow(Priority.NEVER);
        buttonRow.setVgrow(Priority.NEVER);
        
        buttonGrid.getColumnConstraints().addAll(buttonColumn1, buttonColumn2, buttonColumn2, buttonColumn2, buttonColumn3);
        buttonGrid.getRowConstraints().add(buttonRow);
        
        Button summary1 = new Button();
        Button summary2 = new Button();
        Button summary5 = new Button();
        Button back = new Button();
        Button catchButton = new Button();
        
        summary1.setId("summary1sel");
        summary2.setId("summary2");
        summary5.setId("summary3");
        back.setId("backbutton");
        catchButton.setId("catch");
        
        summary1.setMinWidth(68);
        summary1.setPrefWidth(68);
        summary1.setMinHeight(40);
        summary1.setPrefHeight(40);
        summary2.setMinWidth(68);
        summary2.setPrefWidth(68);
        summary2.setMinHeight(40);
        summary2.setPrefHeight(40);
        summary5.setMinWidth(68);
        summary5.setPrefWidth(68);
        summary5.setMinHeight(40);
        summary5.setPrefHeight(40);
        catchButton.setMinWidth(68);
        catchButton.setPrefWidth(68);
        catchButton.setMinHeight(40);
        catchButton.setPrefHeight(40);

        back.setMinWidth(28);
        back.setPrefWidth(28);
        back.setMinHeight(28);
        back.setPrefHeight(28);
        
        buttonGrid.add(summary1, 1, 1);
        buttonGrid.add(summary2, 2, 1);
        buttonGrid.add(summary5, 3, 1);
        buttonGrid.add(catchButton, 4, 1);
        buttonGrid.add(back, 5, 1);
        
        summaryPane.add(labelGrid, 0, 1);
        summaryPane.add(labelValueGrid, 1, 1);
        summaryPane.add(buttonGrid, 0, 3);
        
        int pokePosition;
        
        if(this.targettedPokemon == -1){
            pokePosition = searchPokeArray(this.globalId);
        }
        
        else{
            pokePosition = this.targettedPokemon;
        }
        
        if(pokePosition == -1){
            JSONCapturer pokeCapturer = new JSONCapturer(new URL("https://pokeapi.co/api/v2/"));
            
            String name = new String(pokeCapturer.returnParameter("name", this.globalId));
            String typeName = new String(pokeCapturer.returnParameter("type", this.globalId));
            String typeName2 = new String(pokeCapturer.returnParameter("type2", this.globalId));
            int number = Integer.valueOf(pokeCapturer.returnParameter("id", this.globalId));
            int attack = Integer.valueOf(pokeCapturer.returnParameter("attack", this.globalId));
            int defense = Integer.valueOf(pokeCapturer.returnParameter("defense", this.globalId));
            int spAtk = Integer.valueOf(pokeCapturer.returnParameter("special-attack", this.globalId));
            int spDef = Integer.valueOf(pokeCapturer.returnParameter("special-defense", this.globalId));
            int speed = Integer.valueOf(pokeCapturer.returnParameter("speed", this.globalId));
            int hp = Integer.valueOf(pokeCapturer.returnParameter("hp", this.globalId));
            int height = Integer.valueOf(pokeCapturer.returnParameter("height", this.globalId));
            int weight = Integer.valueOf(pokeCapturer.returnParameter("weight", this.globalId));
            String description = new String(pokeCapturer.returnParameter("flavor_text", this.globalId));
            ArrayList<String> moves = new ArrayList<String>();
            
            int c = 0;
            String move;
            
            if(Integer.valueOf(pokeCapturer.returnParameter("movesize", this.globalId)) > 3){
                while(c < 4){
                    move = pokeCapturer.returnParameter("move", this.globalId);
                    boolean repeat = true;
                
                    if(c != 0){
                        element:
                        for(int x = c - 1; x >= 0; x--){
                            if(move.equals(moves.get(x))){
                                break element;
                            }
                        
                            if((!move.equals(moves.get(x)))&&(x == 0)){
                                repeat = false;
                            }
                        }
                    }
                
                    else{
                        moves.add(move.substring(0, 1).toUpperCase() + move.substring(1));
                        c++;
                    }
                
                    if(!repeat){
                    moves.add(move.substring(0, 1).toUpperCase() + move.substring(1));
                    c++;
                    }
                }
            }
            
            else{
                while(c < Integer.valueOf(pokeCapturer.returnParameter("movesize", this.globalId))){
                    move = pokeCapturer.returnParameter("move", this.globalId);
                    boolean repeat = true;
                
                    if(c != 0){
                        element:
                        for(int x = c - 1; x >= 0; x--){
                            if(move.equals(moves.get(x))){
                                break element;
                            }
                        
                            if((!move.equals(moves.get(x)))&&(x == 0)){
                                repeat = false;
                            }
                        }
                    }
                
                    else{
                        moves.add(move.substring(0, 1).toUpperCase() + move.substring(1));
                        c++;
                    }
                
                    if(!repeat){
                    moves.add(move.substring(0, 1).toUpperCase() + move.substring(1));
                    c++;
                    }
                }
                
                while(c < 4){
                    moves.add("-");
                    c++;
                }
            }

            name = name.substring(0, 1).toUpperCase() + name.substring(1);
            typeName = typeName.substring(0, 1).toUpperCase() + typeName.substring(1);
            
            BufferedImage croppedImage = cropImage(SwingFXUtils.fromFXImage(this.pokemonHollowList.get(this.globalPos).getPokeImage(), null));
            Image pokeImage = SwingFXUtils.toFXImage(croppedImage, null);
            
            //PixelReader resizing sample from jewelsea on Github gist
                        int imWidth = (int) pokeImage.getWidth();
                        int imHeight = (int) pokeImage.getHeight();
    
                        WritableImage output = new WritableImage(imWidth * 2, imHeight * 2);
    
                        PixelReader reader = pokeImage.getPixelReader();
                        PixelWriter writer = output.getPixelWriter();

                        for (int y = 0; y < imHeight; y++) {
                            for (int x = 0; x < imWidth; x++) {
                                final int argb = reader.getArgb(x, y);
                                for (int dy = 0; dy < 2; dy++) {
                                    for (int dx = 0; dx < 2; dx++) {
                                        writer.setArgb(x * 2 + dx, y * 2 + dy, argb);
                                    }
                                }
                            }
                        }
                //PixelReader resizing sample from jewelsea on Github gist
            
            this.pokemonList.add(new Pokemon(name, "-", typeName, typeName2, this.globalId, hp, attack, defense, spAtk, spDef, speed, height, weight, description, output, moves));
            
            pokePosition = this.pokemonList.size() - 1;
        }
        
        Label dexNoValue = new Label(" " + Integer.toString(this.pokemonList.get(pokePosition).getNumber()));
        Label pokeNameValue = new Label(" " + this.pokemonList.get(pokePosition).getName());
        Label pokeTypesValue = new Label(" " + this.pokemonList.get(pokePosition).getTypeName());
        Label pokeOwnerValue = new Label(" " + this.pokemonList.get(pokePosition).getOwner());
        Label ownerIDValue;
        if(this.targettedPokemon == -1){
            ownerIDValue = new Label(" " + "XXXXXX");
        }
        else{
            ownerIDValue = new Label(" " + Integer.toString(this.trainerArray.get(this.currentTrainer).getId()));
        }
        
        dexNoValue.setId("answerLabel");
        pokeNameValue.setId("answerLabel");
        pokeTypesValue.setId("answerLabel");
        pokeOwnerValue.setId("answerLabel");
        ownerIDValue.setId("answerLabel");
          
        labelValueGrid.add(dexNoValue, 0, 0);
        labelValueGrid.add(pokeNameValue, 0, 1);
        labelValueGrid.add(pokeTypesValue, 0, 2);
        labelValueGrid.add(pokeOwnerValue, 0, 3);
        labelValueGrid.add(ownerIDValue, 0, 4);
        
        
        ImageView pokemonImage = new ImageView();
        pokemonImage.setImage(this.pokemonList.get(pokePosition).getImage());
        
        GridPane pokeImagePos = new GridPane();
        
        RowConstraints imageCons1 = new RowConstraints();
        ColumnConstraints imageCons2 = new ColumnConstraints();
        
        imageCons1.setMinHeight(140);
        imageCons2.setMinWidth(30);
        
        pokeImagePos.getRowConstraints().add(imageCons1);
        pokeImagePos.getColumnConstraints().add(imageCons2);
        pokeImagePos.add(pokemonImage, 1, 1);
        
        summaryPane.add(pokeImagePos, 2, 1);
        
        GridPane summaryPane2 = new GridPane();
        summaryPane2.setGridLinesVisible(false);
        
        ColumnConstraints column01 = new ColumnConstraints();
        ColumnConstraints column02 = new ColumnConstraints();
        ColumnConstraints column03 = new ColumnConstraints();
        ColumnConstraints imagePos = new ColumnConstraints();
        
        RowConstraints row01 = new RowConstraints();
        RowConstraints row02 = new RowConstraints();
        RowConstraints row03 = new RowConstraints();
        RowConstraints row07 = new RowConstraints();
        RowConstraints middleRow = new RowConstraints();
        RowConstraints imagePos2 = new RowConstraints();
        
        column01.setMinWidth(16);
        column02.setMinWidth(273);
        column03.setMinWidth(223);
        imagePos.setMinWidth(55);
        column01.setHgrow(Priority.NEVER);
        column02.setHgrow(Priority.NEVER);
        column03.setHgrow(Priority.NEVER);
        imagePos.setHgrow(Priority.NEVER);
        
        row01.setMinHeight(32);
        row02.setMinHeight(31);
        row03.setMinHeight(66);
        row07.setMinHeight(41);
        middleRow.setMinHeight(336);
        imagePos2.setMinHeight(67);
        
        row01.setVgrow(Priority.NEVER);
        row02.setVgrow(Priority.NEVER);
        row03.setVgrow(Priority.NEVER);
        row07.setVgrow(Priority.NEVER);
        middleRow.setVgrow(Priority.NEVER);
        imagePos2.setVgrow(Priority.NEVER);
        
        GridPane middleGrid = new GridPane();
        middleGrid.setGridLinesVisible(false);
        
        middleGrid.getRowConstraints().addAll(row02, row03, row03, row03, row03, row07);
        middleGrid.getColumnConstraints().addAll(column01, column02, column03);
        
        GridPane pokePosGrid = new GridPane();
        pokePosGrid.getColumnConstraints().add(imagePos);
        pokePosGrid.getRowConstraints().add(imagePos2);
        pokePosGrid.setGridLinesVisible(false);
        
        ImageView pokemonImage2 = new ImageView();
        pokemonImage2.setImage(this.pokemonList.get(pokePosition).getImage());
        
        pokePosGrid.add(pokemonImage2, 1, 1);
        
        middleGrid.add(pokePosGrid, 2, 2);
        
        summaryPane2.getRowConstraints().addAll(row01, middleRow, row3);
        
        Button button1 = new Button(this.pokemonList.get(pokePosition).getMoves().get(0));
        Button button2 = new Button(this.pokemonList.get(pokePosition).getMoves().get(1));
        Button button3 = new Button(this.pokemonList.get(pokePosition).getMoves().get(2));
        Button button4 = new Button(this.pokemonList.get(pokePosition).getMoves().get(3));
        
        button1.setMinHeight(66);
        button1.setPrefHeight(66);
        button1.setMinWidth(273);
        button1.setPrefWidth(273);
        button2.setMinHeight(66);
        button2.setPrefHeight(66);
        button2.setMinWidth(273);
        button2.setPrefWidth(273);
        button3.setMinHeight(66);
        button3.setPrefHeight(66);
        button3.setMinWidth(273);
        button3.setPrefWidth(273);
        button4.setMinHeight(66);
        button4.setPrefHeight(66);
        button4.setMinWidth(273);
        button4.setPrefWidth(273);
        
        button1.setId("movebutton");
        button2.setId("movebutton");
        button3.setId("movebutton");
        button4.setId("movebutton");
        
        middleGrid.add(button1, 1, 1);
        middleGrid.add(button2, 1, 2);
        middleGrid.add(button3, 1, 3);
        middleGrid.add(button4, 1, 4);
        
        summaryPane2.add(middleGrid, 0, 1);
        
        GridPane buttonGrid2 = new GridPane();
        
        buttonGrid2.getColumnConstraints().addAll(buttonColumn1, buttonColumn2, buttonColumn2, buttonColumn2, buttonColumn3);
        buttonGrid2.getRowConstraints().add(buttonRow);
        
        Button summary3 = new Button();
        Button summary4 = new Button();
        Button summary5Clone = new Button();
        Button back2 = new Button();
        Button catchButton2 = new Button();
        
        summary3.setId("summary1");
        summary4.setId("summary2sel");
        summary5Clone.setId("summary3");
        back2.setId("backbutton");
        catchButton2.setId("catch");
        
        summary3.setMinWidth(68);
        summary3.setPrefWidth(68);
        summary3.setMinHeight(40);
        summary3.setPrefHeight(40);
        summary4.setMinWidth(68);
        summary4.setPrefWidth(68);
        summary4.setMinHeight(40);
        summary4.setPrefHeight(40);
        summary5Clone.setMinWidth(68);
        summary5Clone.setPrefWidth(68);
        summary5Clone.setMinHeight(40);
        summary5Clone.setPrefHeight(40);
        catchButton2.setMinWidth(68);
        catchButton2.setPrefWidth(68);
        catchButton2.setMinHeight(40);
        catchButton2.setPrefHeight(40);
        back2.setMinWidth(28);
        back2.setPrefWidth(28);
        back2.setMinHeight(28);
        back2.setPrefHeight(28);
        
        buttonGrid2.add(summary3, 1, 1);
        buttonGrid2.add(summary4, 2, 1);
        buttonGrid2.add(summary5Clone, 3, 1);
        buttonGrid2.add(catchButton2, 4, 1);
        buttonGrid2.add(back2, 5, 1);
        
        summaryPane2.add(buttonGrid2, 0, 2);
        
        GridPane summaryPane3 = new GridPane();
        summaryPane3.setId("summary3scene");
        
        RowConstraints pane3Row1 = new RowConstraints();
        RowConstraints pane3Row2 = new RowConstraints();
        RowConstraints pane3Row3 = new RowConstraints();
        RowConstraints pane3Row4 = new RowConstraints();
        
        pane3Row1.setMinHeight(41);
        pane3Row2.setMinHeight(199);
        pane3Row3.setMinHeight(108);
        pane3Row4.setMinHeight(20);
        pane3Row1.setVgrow(Priority.NEVER);
        pane3Row2.setVgrow(Priority.NEVER);
        pane3Row3.setVgrow(Priority.NEVER);
        
        summaryPane3.getRowConstraints().addAll(pane3Row1, pane3Row2, pane3Row3, pane3Row4, row4);
        
        GridPane pane3Divisory1 = new GridPane();
        
        ColumnConstraints pane3D1Column1 = new ColumnConstraints();
        ColumnConstraints pane3D1Column2 = new ColumnConstraints();
        
        pane3D1Column1.setMinWidth(252);
        pane3D1Column1.setMinWidth(260);
        
        pane3Divisory1.getColumnConstraints().addAll(pane3D1Column1, pane3D1Column2);
        
        Pane pane3ImagePos = new Pane();
        
        ImageView pokemonImage3 = new ImageView();
        pokemonImage3.setImage(this.pokemonList.get(pokePosition).getImage());
        pokemonImage3.setLayoutX(49);
        pokemonImage3.setLayoutY(31);
        
        pane3ImagePos.getChildren().add(pokemonImage3);
        
        GridPane pane3Info = new GridPane();
        
        RowConstraints pane3InfoRow1 = new RowConstraints();
        pane3InfoRow1.setMinHeight(40);
        pane3InfoRow1.setVgrow(Priority.NEVER);
        RowConstraints pane3InfoRow2 = new RowConstraints();
        pane3InfoRow2.setMinHeight(91);
        pane3InfoRow2.setVgrow(Priority.NEVER);
        RowConstraints pane3InfoRow3 = new RowConstraints();
        pane3InfoRow3.setMinHeight(68);
        pane3InfoRow3.setVgrow(Priority.NEVER);
        
        pane3Info.getRowConstraints().addAll(pane3InfoRow1, pane3InfoRow2, pane3InfoRow3);
        
        Label pane3Label1 = new Label("    " + this.pokemonList.get(pokePosition).getName());
        Label hpLabel = new Label("HP: " + Integer.toString(this.pokemonList.get(pokePosition).getHp()));
        Label attackLabel = new Label("ATK: " + Integer.toString(this.pokemonList.get(pokePosition).getAttack()));
        Label speedLabel = new Label("SPD: " + Integer.toString(this.pokemonList.get(pokePosition).getSpeed()));
        Label defLabel = new Label("DEF: " + Integer.toString(this.pokemonList.get(pokePosition).getDefense()));
        Label spAttackLabel = new Label("SpATK: " + Integer.toString(this.pokemonList.get(pokePosition).getSpAtk()));
        Label spDefLabel = new Label("SpATK: " + Integer.toString(this.pokemonList.get(pokePosition).getSpDef()));
        Label heightLabel = new Label("    Height: " + Integer.toString(this.pokemonList.get(pokePosition).getHeight()));
        Label weightLabel = new Label("    Weight: " + Integer.toString(this.pokemonList.get(pokePosition).getWeight()));
        Label descriptionLabel = new Label(this.pokemonList.get(pokePosition).getDescription());
        
        pane3Label1.setId("summary3label");
        hpLabel.setId("attributeslabel");
        attackLabel.setId("attributeslabel");
        speedLabel.setId("attributeslabel");
        defLabel.setId("attributeslabel");
        spAttackLabel.setId("attributeslabel");
        spDefLabel.setId("attributeslabel");
        heightLabel.setId("summary3label");
        weightLabel.setId("summary3label");
        descriptionLabel.setId("description");
        descriptionLabel.setLayoutY(18);
        descriptionLabel.setLayoutX(10);
        
        VBox attributesBox1 = new VBox(hpLabel, attackLabel, speedLabel);
        attributesBox1.setSpacing(2);
        attributesBox1.setAlignment(Pos.CENTER_LEFT);
        VBox attributesBox2 = new VBox(defLabel, spAttackLabel, spDefLabel);
        attributesBox2.setSpacing(2);
        attributesBox2.setAlignment(Pos.CENTER_RIGHT);
        
        Pane descriptionPane = new Pane();
        descriptionPane.getChildren().add(descriptionLabel);
        descriptionPane.setMaxWidth(512);
       // descriptionPane.setMaxHeight(105);
        
        VBox pane3VBox = new VBox(heightLabel, weightLabel);
        pane3VBox.setSpacing(6);
        
        pane3Info.add(pane3Label1, 0, 0);
        pane3Info.add(attributesBox1, 0, 1);
        pane3Info.add(attributesBox2, 0, 1);
        pane3Info.add(pane3VBox, 0, 2);
        
        pane3Divisory1.add(pane3ImagePos, 0, 0);
        pane3Divisory1.add(pane3Info, 1, 0);
        
        summaryPane3.add(pane3Divisory1, 0, 1);
        summaryPane3.add(descriptionPane, 0, 2);
        
        Button summary1Clone = new Button();
        Button summary2Clone = new Button();
        Button summary5Clone2 = new Button();
        Button backClone2 = new Button();
        Button catchButton3 = new Button();
        
        summary1Clone.setId("summary1");
        summary2Clone.setId("summary2");
        summary5Clone2.setId("summary3");
        backClone2.setId("backbutton");
        catchButton3.setId("catch");
        
        summary1Clone.setMinWidth(68);
        summary1Clone.setPrefWidth(68);
        summary1Clone.setMinHeight(40);
        summary1Clone.setPrefHeight(40);
        summary2Clone.setMinWidth(68);
        summary2Clone.setPrefWidth(68);
        summary2Clone.setMinHeight(40);
        summary2Clone.setPrefHeight(40);
        summary5Clone2.setMinWidth(68);
        summary5Clone2.setPrefWidth(68);
        summary5Clone2.setMinHeight(40);
        summary5Clone2.setPrefHeight(40);
        catchButton3.setMinWidth(68);
        catchButton3.setPrefWidth(68);
        catchButton3.setMinHeight(40);
        catchButton3.setPrefHeight(40);

        backClone2.setMinWidth(28);
        backClone2.setPrefWidth(28);
        backClone2.setMinHeight(28);
        backClone2.setPrefHeight(28);
        
        GridPane buttonGrid3 = new GridPane();
        
        buttonGrid3.getColumnConstraints().addAll(buttonColumn1, buttonColumn2, buttonColumn2, buttonColumn2, buttonColumn3);
        buttonGrid3.getRowConstraints().add(buttonRow);
        
        buttonGrid3.add(summary1Clone, 1, 1);
        buttonGrid3.add(summary2Clone, 2, 1);
        buttonGrid3.add(summary5Clone2, 3, 1);
        buttonGrid3.add(catchButton3, 4, 1);
        buttonGrid3.add(backClone2, 5, 1);
        
        
        summaryPane3.add(buttonGrid3, 0, 4);

        Scene summaryScene = new Scene(summaryPane);
        summaryScene.getStylesheets().add(getClass().getResource("gridStyle.css").toExternalForm());
        
        final Scene summaryPane2Scene = new Scene(summaryPane2);
        summaryPane2Scene.getStylesheets().add(getClass().getResource("gridStyle.css").toExternalForm());
        
        Scene summaryPane3Scene = new Scene(summaryPane3);
        summaryPane3Scene.getStylesheets().add(getClass().getResource("gridStyle.css").toExternalForm());
        
        MediaPlayer backSound = new MediaPlayer(new Media((new File("src/resources/sounds/Back.wav").toURI().toString())));
        MediaPlayer confirmSound = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm.wav").toURI().toString())));
        MediaPlayer catchSound = new MediaPlayer(new Media((new File("src/resources/sounds/Catch.wav").toURI().toString())));
        MediaPlayer errorSound = new MediaPlayer(new Media((new File("src/resources/sounds/Error.wav").toURI().toString())));
        backSound.setVolume(0.5);
        
        final Pokemon potentialCatch = this.pokemonList.get(pokePosition);
        if(this.currentTrainer != -1){
            potentialCatch.setOwner(this.trainerArray.get(currentTrainer).getName());
        }
        
        EventHandler<MouseEvent> catchHandler = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
               if(currentTrainer != -1){
                catchSound.play();
                trainerArray.get(currentTrainer).getPokemonList().add(new Pokemon(potentialCatch.getName(), potentialCatch.getOwner(), potentialCatch.getTypeName(), potentialCatch.getTypeName2(), potentialCatch.getNumber(), potentialCatch.getHp(), potentialCatch.getAttack(), potentialCatch.getDefense(), potentialCatch.getSpAtk(), potentialCatch.getSpDef(), potentialCatch.getSpeed(), potentialCatch.getHeight(), potentialCatch.getWeight(), potentialCatch.getDescription(), potentialCatch.getImage(), potentialCatch.getMoves()));
                trainerArray.get(currentTrainer).setPokemonNumber(trainerArray.get(currentTrainer).getPokemonNumber() + 1);
                catchSound.seek(Duration.ZERO);
               }
               else{
                errorSound.play();
                errorSound.seek(Duration.ZERO);
               }
            }
        };
        
        EventHandler<MouseEvent> summary1Handler = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
               confirmSound.play();
               pokeWindow.setScene(summaryScene);
               confirmSound.seek(Duration.ZERO);
            }
        };
        
        EventHandler<MouseEvent> summary2Handler = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                confirmSound.play();
                pokeWindow.setScene(summaryPane2Scene);
                confirmSound.seek(Duration.ZERO);
            }
        };
        
        EventHandler<MouseEvent> summary3Handler = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                confirmSound.play();
                pokeWindow.setScene(summaryPane3Scene);
                confirmSound.seek(Duration.ZERO);
            }
        };
        
        EventHandler<MouseEvent> backHandler = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                backSound.play();
                pokeWindow.setScene(mainScene);
                backSound.seek(Duration.ZERO);
            }
        };
        
        summary3.setOnMouseClicked(summary1Handler);
        summary1Clone.setOnMouseClicked(summary1Handler);
        
        summary2.setOnMouseClicked(summary2Handler);
        summary2Clone.setOnMouseClicked(summary2Handler);
       
        summary5.setOnMouseClicked(summary3Handler);
        summary5Clone.setOnMouseClicked(summary3Handler);
        
        back.setOnMouseClicked(backHandler);
        back2.setOnMouseClicked(backHandler);
        backClone2.setOnMouseClicked(backHandler);
        
        catchButton.setOnMouseClicked(catchHandler);
        catchButton2.setOnMouseClicked(catchHandler);
        catchButton3.setOnMouseClicked(catchHandler);
        
        this.currentScene = summaryScene;
    }
    
    private void setTrainerScene() throws Exception {
        MediaPlayer confirmSound = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm.wav").toURI().toString())));
        MediaPlayer confirm2Sound = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm2.wav").toURI().toString())));
        MediaPlayer backSound = new MediaPlayer(new Media((new File("src/resources/sounds/Back.wav").toURI().toString())));
        MediaPlayer errorSound = new MediaPlayer(new Media((new File("src/resources/sounds/Error.wav").toURI().toString())));
        backSound.setVolume(0.5);
        confirm2Sound.setVolume(0.5);
        
        GridPane root = new GridPane();
        
        RowConstraints mainRow1 = new RowConstraints();
        RowConstraints mainRow2 = new RowConstraints();
        RowConstraints mainRow3 = new RowConstraints();
        
        mainRow1.setMinHeight(18);
        mainRow2.setMinHeight(350);
        mainRow3.setMinHeight(48);
        mainRow1.setVgrow(Priority.NEVER);
        mainRow2.setVgrow(Priority.NEVER);
        mainRow3.setVgrow(Priority.NEVER);
        
        GridPane middleSpace = new GridPane();
        
        ColumnConstraints middleColumn1 = new ColumnConstraints();
        ColumnConstraints middleColumn2 = new ColumnConstraints();
        ColumnConstraints middleColumn3 = new ColumnConstraints();
        
        middleColumn1.setMinWidth(17);
        middleColumn2.setMinWidth(212);
        middleColumn3.setMinWidth(283);
        middleColumn1.setHgrow(Priority.NEVER);
        middleColumn2.setHgrow(Priority.NEVER);
        middleColumn3.setHgrow(Priority.NEVER);
        
        GridPane trainerRegister = new GridPane();
        trainerRegister.setMinSize(283, 320);
        trainerRegister.setPrefSize(283, 320);
        trainerRegister.setMaxSize(283, 320);
        trainerRegister.setId("register");
        
        RowConstraints trainerRegisterRow1 = new RowConstraints();
        RowConstraints trainerRegisterRow2 = new RowConstraints();
        RowConstraints trainerRegisterRow3 = new RowConstraints();
        
        trainerRegisterRow1.setMinHeight(130);
        trainerRegisterRow1.setPrefHeight(130);
        trainerRegisterRow2.setMinHeight(161);
        trainerRegisterRow2.setPrefHeight(161);
        trainerRegisterRow3.setMinHeight(29);
        trainerRegisterRow3.setPrefHeight(29);
        
        TextField trainerNameField = new TextField("13 characters max.");
        trainerNameField.setId("registerfield");
        trainerNameField.setMinWidth(260);
        trainerNameField.setPrefWidth(260);
        trainerNameField.setMaxWidth(260);
        trainerNameField.setMinHeight(26);
        trainerNameField.setPrefHeight(26);
        trainerNameField.setMaxHeight(26);
        
        trainerRegister.getRowConstraints().addAll(trainerRegisterRow1, trainerRegisterRow2, trainerRegisterRow3);
        
        Pane textFieldPane = new Pane();
        textFieldPane.getChildren().add(trainerNameField);
        trainerNameField.setLayoutX(12);
        trainerNameField.setLayoutY(40);
        
        trainerRegister.add(textFieldPane, 0, 0);
        
        Button redIcon = new Button();
        Button hilbertIcon = new Button();
        Button elioIcon = new Button();
        Button leafIcon = new Button();
        Button hildaIcon = new Button();
        Button moonIcon = new Button();
                
        redIcon.setId("redicon");
        hilbertIcon.setId("hilberticon");
        elioIcon.setId("elioicon");
        leafIcon.setId("leaficon");
        hildaIcon.setId("hildaicon");
        moonIcon.setId("moonicon");
        
        redIcon.setMinSize(79, 79);
        redIcon.setPrefSize(79, 79);
        redIcon.setMaxSize(79, 79);
        hilbertIcon.setMinSize(79, 79);
        hilbertIcon.setPrefSize(79, 79);
        hilbertIcon.setMaxSize(79, 79);
        elioIcon.setMinSize(79, 79);
        elioIcon.setPrefSize(79, 79);
        elioIcon.setMaxSize(79, 79);
        leafIcon.setMinSize(79, 79);
        leafIcon.setPrefSize(79, 79);
        leafIcon.setMaxSize(79, 79);
        hildaIcon.setMinSize(79, 79);
        hildaIcon.setPrefSize(79, 79);
        hildaIcon.setMaxSize(79, 79);
        moonIcon.setMinSize(79, 79);
        moonIcon.setPrefSize(79, 79);
        moonIcon.setMaxSize(79, 79);
        
        redIcon.setOnMouseClicked(e -> targettedTrainer = "red");
        hilbertIcon.setOnMouseClicked(e -> targettedTrainer = "hilbert");
        elioIcon.setOnMouseClicked(e -> targettedTrainer = "elio");
        leafIcon.setOnMouseClicked(e -> targettedTrainer = "leaf");
        hildaIcon.setOnMouseClicked(e -> targettedTrainer = "hilda");
        moonIcon.setOnMouseClicked(e -> targettedTrainer = "moon");
        
        HBox boys = new HBox(redIcon, hilbertIcon, elioIcon);
        HBox girls = new HBox(leafIcon, hildaIcon, moonIcon);
        boys.setSpacing(2);
        girls.setSpacing(2);
        
        Pane iconPane = new Pane();
        iconPane.getChildren().addAll(boys, girls);
        boys.setLayoutX(21);
        girls.setLayoutX(21);
        girls.setLayoutY(82);
        
        trainerRegister.add(iconPane, 0, 1);
        
        Button registerConfirm = new Button();
        registerConfirm.setId("registerconfirm");
        registerConfirm.setMinSize(90, 24);
        registerConfirm.setPrefSize(90, 24);
        registerConfirm.setMaxSize(90, 24);
        
        Pane registerConfirmationPane = new Pane();
        registerConfirmationPane.getChildren().add(registerConfirm);
        registerConfirm.setLayoutY(5);
        registerConfirm.setLayoutX(94);
        
        trainerRegister.add(registerConfirmationPane, 0, 2);
        
        Scene trainerRegisterScene = new Scene(trainerRegister);
        trainerRegisterScene.getStylesheets().add(getClass().getResource("trainerStyle.css").toExternalForm());
        
        
        Stage registerPopUp = new Stage();
        registerPopUp.setScene(trainerRegisterScene);
        registerPopUp.setMinHeight(348);
        registerPopUp.setMinWidth(283);
        registerPopUp.setMaxHeight(348);
        registerPopUp.setMaxWidth(283);
        registerPopUp.setResizable(false);
        
        ImageView trainerImage = new ImageView();
        trainerImage.setImage(new Image(new File("src/resources/tiles/transparent.png").toURI().toString()));
        
        Label nameTrainer = new Label("Name:");
        Label nameTrainerAnswer = new Label();
        Label pokemonNumber = new Label("Pokemons:");
        Label pokemonNumberAnswer = new Label();
        
        nameTrainer.setId("trainerinfo");
        nameTrainerAnswer.setId("trainerinfoanswer");
        pokemonNumber.setId("trainerinfo");
        pokemonNumberAnswer.setId("trainerinfoanswer");
        
        HBox nameAttribute = new HBox(nameTrainer, nameTrainerAnswer);
        nameAttribute.setSpacing(1);
        HBox numberAttribute = new HBox(pokemonNumber, pokemonNumberAnswer);
        numberAttribute.setSpacing(1);
        
        VBox attributes = new VBox(nameAttribute, numberAttribute);
        attributes.setSpacing(30);
        attributes.setTranslateY(68);
        attributes.setTranslateX(2);
        attributes.setVisible(false);
        
        GridPane trainerImagePane = new GridPane();
        trainerImagePane.add(trainerImage, 0, 0);
        trainerImagePane.add(attributes, 0, 0);
        trainerImagePane.setAlignment(Pos.BOTTOM_CENTER);
        
        ArrayList<Button> trainerButtonsArray = new ArrayList<Button>();
        
        if(justStarted){
            readTrainersFile();
            
            for(int c = 0; c < this.trainerArray.size(); c++){
                for(int z = 0; z < this.trainerArray.get(c).getPokemonList().size(); z++){
                    BufferedImage imageCropped = cropImage(SwingFXUtils.fromFXImage(this.trainerArray.get(c).getPokemonList().get(z).getImage(), null));
                    Image croppedImage = SwingFXUtils.toFXImage(imageCropped, null);
                    
                //PixelReader resizing sample from jewelsea on Github gist
                        int imWidth = (int) croppedImage.getWidth();
                        int imHeight = (int) croppedImage.getHeight();
    
                        WritableImage output = new WritableImage(imWidth * 2, imHeight * 2);
    
                        PixelReader reader = croppedImage.getPixelReader();
                        PixelWriter writer = output.getPixelWriter();

                        for (int y = 0; y < imHeight; y++) {
                            for (int x = 0; x < imWidth; x++) {
                                final int argb = reader.getArgb(x, y);
                                for (int dy = 0; dy < 2; dy++) {
                                    for (int dx = 0; dx < 2; dx++) {
                                        writer.setArgb(x * 2 + dx, y * 2 + dy, argb);
                                    }
                                }
                            }
                        }
                //PixelReader resizing sample from jewelsea on Github gist
                
                this.trainerArray.get(c).getPokemonList().get(z).setImage(output);
                }
            }
        }
        
            for(int c = 0; c < 5; c++){
                if(c < this.trainerArray.size()){
                    final int index = c;

                    Button newTrainerButton = new Button(this.trainerArray.get(c).getName());
                    newTrainerButton.setId("trainersave");
                    newTrainerButton.setMinSize(212, 64);
                    newTrainerButton.setPrefSize(212, 64);
                    newTrainerButton.setMaxSize(212, 64);
                    newTrainerButton.setOnMouseEntered(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent event) {
                            trainerImage.setImage(trainerArray.get(index).getImage());
                            nameTrainerAnswer.setText(trainerArray.get(index).getName());
                            pokemonNumberAnswer.setText(Integer.toString(trainerArray.get(index).getPokemonNumber()));
                            attributes.setVisible(true);
                        }
                    });

                    newTrainerButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent event) {
                            confirm2Sound.play();
                            currentTrainer = index;
                            confirm2Sound.seek(Duration.ZERO);
                            try {
                                setTrainerProfile(index);
                            } catch (Exception ex) {
                                Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    trainerButtonsArray.add(newTrainerButton);
                }

                else{
                    final int aux = c;
                    Button temporaryButton = new Button("Create new\n  trainer!");
                    temporaryButton.setId("notrainersave");
                    temporaryButton.setMinSize(212, 64);
                    temporaryButton.setPrefSize(212, 64);
                    temporaryButton.setMaxSize(212, 64);
                    temporaryButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent event) {
                        confirm2Sound.play();
                        targettedButton = aux;
                        registerPopUp.show();
                        confirm2Sound.seek(Duration.ZERO);
                    }
                });
                trainerButtonsArray.add(temporaryButton);
                }
            }
        
        this.justStarted = false;
        
        VBox trainersButtonBox = new VBox();
        trainersButtonBox.getChildren().addAll(trainerButtonsArray);
        
        registerConfirm.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if((trainerNameField.getCharacters().toString().length() < 14)&&(targettedTrainer != null)) {
                    Trainer newTrainer = new Trainer(trainerNameField.getCharacters().toString(), targettedTrainer, 0);
                    
                    int index = trainerArray.size();
                    trainerArray.add(newTrainer);
                    
                    Button newTrainerButton = new Button(newTrainer.getName());
                    newTrainerButton.setId("trainersave");
                    newTrainerButton.setMinSize(212, 64);
                    newTrainerButton.setPrefSize(212, 64);
                    newTrainerButton.setMaxSize(212, 64);
                    newTrainerButton.setOnMouseEntered(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent event) {
                            trainerImage.setImage(trainerArray.get(index).getImage());
                            nameTrainerAnswer.setText(trainerArray.get(index).getName());
                            pokemonNumberAnswer.setText(Integer.toString(trainerArray.get(index).getPokemonNumber()));
                            attributes.setVisible(true);
                        }
                    });
                    newTrainerButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent event) {
                                confirm2Sound.play();
                                currentTrainer = index;
                                confirm2Sound.seek(Duration.ZERO);
                            try {
                                setTrainerProfile(index);
                            } catch (Exception ex) {
                                Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    trainersButtonBox.getChildren().remove(trainerButtonsArray.get(targettedButton));
                    trainerButtonsArray.set(targettedButton, newTrainerButton);
                    trainersButtonBox.getChildren().add(targettedButton, trainerButtonsArray.get(targettedButton));
                    
                    registerPopUp.close();
                }
                
                else {
                    errorSound.play();
                    errorSound.seek(Duration.ZERO);
                }
            }
        });
                
        middleSpace.getColumnConstraints().addAll(middleColumn1, middleColumn2, middleColumn3);
        middleSpace.add(trainersButtonBox, 1, 0);
        middleSpace.add(trainerImagePane, 2, 0);
        
        Button back = new Button();
        back.setMinWidth(28);
        back.setPrefWidth(28);
        back.setMinHeight(28);
        back.setPrefHeight(28);
        back.setId("back");

        back.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                backSound.play();
                backSound.seek(Duration.ZERO);
                pokeWindow.setScene(mainScene);
                
            }
        });
        
        GridPane lowerButtons = new GridPane();
        lowerButtons.add(back, 0, 0);
        lowerButtons.setAlignment(Pos.CENTER_RIGHT);
        
        root.getRowConstraints().addAll(mainRow1, mainRow2, mainRow3);
        root.add(middleSpace, 0, 1);
        root.add(lowerButtons, 0, 2);
        
        back.setTranslateX(-20);
        
        Scene trainerScene = new Scene(root);
        trainerScene.getStylesheets().add(getClass().getResource("trainerStyle.css").toExternalForm());
        
        this.pokeWindow.setScene(trainerScene);
    }
    
    private void setTrainerProfile(int index) throws Exception {
        GridPane root = new GridPane();
        Stage profileStage = new Stage();
        
        MediaPlayer confirmSound = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm.wav").toURI().toString())));
        MediaPlayer hoverSound = new MediaPlayer(new Media((new File("src/resources/sounds/SelectionMove.wav").toURI().toString())));
        MediaPlayer backSound = new MediaPlayer(new Media((new File("src/resources/sounds/Back.wav").toURI().toString())));
        backSound.setVolume(0.5);
        
        RowConstraints rootRow1 = new RowConstraints();
        RowConstraints rootRow2 = new RowConstraints();
        RowConstraints rootRow3 = new RowConstraints();
        
        rootRow1.setMinHeight(12);
        rootRow1.setPrefHeight(12);
        rootRow1.setMaxHeight(12);
        rootRow2.setMinHeight(356);
        rootRow2.setPrefHeight(356);
        rootRow2.setMaxHeight(356);
        rootRow3.setMinHeight(48);
        rootRow3.setPrefHeight(48);
        rootRow3.setMaxHeight(48);
        rootRow1.setVgrow(Priority.NEVER);
        rootRow2.setVgrow(Priority.NEVER);
        rootRow3.setVgrow(Priority.NEVER);
        
        root.getRowConstraints().addAll(rootRow1, rootRow2, rootRow3);
        
        GridPane middlePane = new GridPane();
        
        ColumnConstraints middlePaneColumn1 = new ColumnConstraints();
        ColumnConstraints middlePaneColumn2 = new ColumnConstraints();
        
        middlePaneColumn1.setMinWidth(313);
        middlePaneColumn1.setPrefWidth(313);
        middlePaneColumn1.setMaxWidth(313);
        middlePaneColumn2.setMinWidth(199);
        middlePaneColumn2.setPrefWidth(199);
        middlePaneColumn2.setMaxWidth(199);
        middlePaneColumn1.setHgrow(Priority.NEVER);
        middlePaneColumn2.setHgrow(Priority.NEVER);
        
        middlePane.getColumnConstraints().addAll(middlePaneColumn1, middlePaneColumn2);
        
        ImageView profilePicture = new ImageView();
        
        if(this.trainerArray.get(index).getImageTarget().equals("red")){
            profilePicture.setId("redicon");
        }
        
        if(this.trainerArray.get(index).getImageTarget().equals("hilbert")){
            profilePicture.setId("hilberticon");
        }
        
        if(this.trainerArray.get(index).getImageTarget().equals("elio")){
            profilePicture.setId("elioicon");
        }
        
        if(this.trainerArray.get(index).getImageTarget().equals("leaf")){
            profilePicture.setId("leaficon");
        }
        
        if(this.trainerArray.get(index).getImageTarget().equals("hilda")){
            profilePicture.setId("hildaicon");
        }
        
        if(this.trainerArray.get(index).getImageTarget().equals("moon")){
            profilePicture.setId("moonicon");
        }
        
        Label name = new Label("Name: " + this.trainerArray.get(index).getName());
        Label pokemons = new Label("Pokemons: " + this.trainerArray.get(index).getPokemonNumber());
        Label idNo = new Label("ID NO.: " + this.trainerArray.get(index).getId());
        
        name.setId("profileinfo");
        pokemons.setId("profileinfo");
        idNo.setId("profileinfo");
        
        VBox labelBox = new VBox(name, pokemons, idNo);
        labelBox.setSpacing(22);
        
        GridPane middleDivider = new GridPane();
        RowConstraints divider1 = new RowConstraints();
        RowConstraints divider2 = new RowConstraints();
        
        divider1.setMinHeight(95);
        divider1.setPrefHeight(95);
        divider1.setMaxHeight(95);
        divider2.setMinHeight(261);
        divider2.setPrefHeight(261);
        divider2.setMaxHeight(261);
        divider1.setVgrow(Priority.NEVER);
        divider2.setVgrow(Priority.NEVER);
        
        ImageView pokemonImage = new ImageView();
        pokemonImage.setImage(new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString()));
        
        middleDivider.getRowConstraints().addAll(divider1, divider2);
        middlePane.add(middleDivider, 0, 0);
        
        middleDivider.add(profilePicture, 0, 0);
        middleDivider.add(labelBox, 0, 0);
        middleDivider.add(pokemonImage, 0, 1);

        profilePicture.setTranslateX(17);
        profilePicture.setTranslateY(-2);
        labelBox.setTranslateX(100);
        labelBox.setTranslateY(-2);
        pokemonImage.setTranslateX((313 - pokemonImage.getImage().getWidth())/2);
        
        ArrayList<Button> pokemonButtons = new ArrayList<Button>();
        
        for(int c = 0; c < this.trainerArray.get(index).getPokemonList().size(); c++){
            final Image pokemonImg = this.trainerArray.get(index).getPokemonList().get(c).getImage();
            final Pokemon pokemon = this.trainerArray.get(index).getPokemonList().get(c);
            
            Button button = new Button(this.trainerArray.get(index).getPokemonList().get(c).getName());
            
            button.setMinSize(187, 42);
            button.setPrefSize(187, 42);
            button.setMaxSize(187, 42);
            button.setId("profilebutton");
            button.setOnMouseEntered(new EventHandler<MouseEvent>(){
                @Override
                public void handle(MouseEvent event) {
                    hoverSound.play();
                    pokemonImage.setImage(pokemonImg);
                    hoverSound.seek(Duration.ZERO);
                }
            });
            button.setOnMouseClicked(new EventHandler<MouseEvent>(){
                @Override
                public void handle(MouseEvent event) {
                    confirmSound.play();
                    targettedPokemon = pokemonList.size();
                    pokemonList.add(pokemon);
                    targetScene = "summaryScene";
                    loadScene();
                    profileStage.close();
                    profileIsOpen = false;
                    confirmSound.seek(Duration.ZERO);
                }
            });
            
            pokemonButtons.add(button);
        }
        
        VBox buttonsBox = new VBox();
        buttonsBox.getChildren().addAll(pokemonButtons);
        buttonsBox.setSpacing(5);
        
        ScrollPane buttonScroll = new ScrollPane();
        buttonScroll.setContent(buttonsBox);
        buttonScroll.setBackground(Background.EMPTY);
        buttonScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        
        middlePane.add(buttonScroll, 1, 0);
        
        Button exit = new Button();
        exit.setMinWidth(28);
        exit.setPrefWidth(28);
        exit.setMinHeight(28);
        exit.setPrefHeight(28);
        exit.setId("exit");
        exit.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                backSound.play();
                profileStage.close();
                profileIsOpen = false;
                backSound.seek(Duration.ZERO);
            }
        });
        
        GridPane exitPane = new GridPane();
        exitPane.add(exit, 0, 0);
        exitPane.setAlignment(Pos.CENTER_RIGHT);
        exit.setTranslateX(-15);

        root.add(middlePane, 0, 1);
        root.add(exitPane, 0, 2);
        
        Scene profileScene = new Scene(root);
        profileScene.getStylesheets().add(getClass().getResource("profileStyle.css").toExternalForm());
        
        profileStage.setScene(profileScene);
        profileStage.setWidth(512);
        profileStage.setHeight(416);
        profileStage.setResizable(false);
        profileStage.initStyle(StageStyle.UNDECORATED);
        profileStage.setOnShown(e -> profileIsOpen = true);
        profileStage.setOnCloseRequest(e -> profileIsOpen = false);
        if(!profileIsOpen){
            profileStage.show();
        }
    }
    
    private void setSearchScene() throws Exception {
        GridPane root = new GridPane();
        
        MediaPlayer confirmSound = new MediaPlayer(new Media((new File("src/resources/sounds/Confirm.wav").toURI().toString())));
        MediaPlayer hoverSound = new MediaPlayer(new Media((new File("src/resources/sounds/SelectionMove.wav").toURI().toString())));
        MediaPlayer backSound = new MediaPlayer(new Media((new File("src/resources/sounds/Back.wav").toURI().toString())));
        backSound.setVolume(0.5);
        
        RowConstraints rootRow1 = new RowConstraints();
        RowConstraints rootRow2 = new RowConstraints();
        RowConstraints rootRow3 = new RowConstraints();
        
        rootRow1.setMinHeight(31);
        rootRow1.setPrefHeight(31);
        rootRow1.setMaxHeight(31);
        rootRow2.setMinHeight(337);
        rootRow2.setPrefHeight(337);
        rootRow2.setMaxHeight(337);
        rootRow3.setMinHeight(48);
        rootRow3.setPrefHeight(48);
        rootRow3.setMaxHeight(48);
        rootRow1.setVgrow(Priority.NEVER);
        rootRow2.setVgrow(Priority.NEVER);
        rootRow3.setVgrow(Priority.NEVER);
        
        root.getRowConstraints().addAll(rootRow1, rootRow2, rootRow3);
        
        GridPane middleGrid = new GridPane();
        ColumnConstraints dividingColumn1 = new ColumnConstraints();
        
        dividingColumn1.setMinWidth(256);
        dividingColumn1.setPrefWidth(256);
        dividingColumn1.setMaxWidth(256);
        dividingColumn1.setHgrow(Priority.NEVER);
        
        middleGrid.getColumnConstraints().addAll(dividingColumn1, dividingColumn1);
        
        GridPane leftGrid = new GridPane();
        GridPane rightGrid = new GridPane();
        RowConstraints leftRow1 = new RowConstraints();
        RowConstraints leftRow2 = new RowConstraints();
        RowConstraints rightRow1 = new RowConstraints();
        RowConstraints rightRow2 = new RowConstraints();
        
        leftRow1.setMinHeight(196);
        leftRow1.setPrefHeight(196);
        leftRow1.setMaxHeight(196);
        leftRow2.setMinHeight(141);
        leftRow2.setPrefHeight(141);
        leftRow2.setMaxHeight(141);
        rightRow1.setMinHeight(67);
        rightRow1.setPrefHeight(67);
        rightRow1.setMaxHeight(67);
        rightRow2.setMinHeight(270);
        rightRow2.setPrefHeight(270);
        rightRow2.setMaxHeight(270);
        
        leftGrid.getRowConstraints().addAll(leftRow1, leftRow2);
        rightGrid.getRowConstraints().addAll(rightRow1, rightRow2);
        
        rightGrid.setAlignment(Pos.CENTER);
        leftGrid.setAlignment(Pos.CENTER);
        
        final ToggleGroup searchOptions = new ToggleGroup();
        
        RadioButton nameOption = new RadioButton("Name");
        RadioButton typeOption = new RadioButton("Type");
        nameOption.setToggleGroup(searchOptions);
        typeOption.setToggleGroup(searchOptions);
        nameOption.setSelected(true);

        HBox optionsBox = new HBox(nameOption, typeOption);
        optionsBox.setSpacing(10);
        optionsBox.setAlignment(Pos.CENTER);
        
        TextField searchInput = new TextField("Ex: pikachu");
        searchInput.setMinSize(232, 34);
        searchInput.setPrefSize(232, 34);
        searchInput.setMaxSize(232, 34);
        
        VBox searchObjects = new VBox(searchInput, optionsBox);
        searchObjects.setSpacing(8);
        searchObjects.setAlignment(Pos.CENTER);
        
        ScrollPane resultScroll = new ScrollPane();
        resultScroll.setBackground(Background.EMPTY);
        
        ImageView resultImage = new ImageView();
        
        searchInput.setOnKeyPressed(new EventHandler<KeyEvent>(){
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode().equals(KeyCode.ENTER)){
                    ArrayList<Button> searchResults = new ArrayList<Button>();
                    if(nameOption.isSelected()){
                        String inputLowercase = searchInput.getCharacters().toString().toLowerCase();
                        
                        if(inputLowercase.equals("ghost")){
                            Button resultButton = new Button("?????");
                            resultButton.setId("resultbutton");
                            resultButton.setMinSize(199, 42);
                            resultButton.setPrefSize(199, 42);
                            resultButton.setMaxSize(199, 42);
                            resultButton.setOnMouseEntered(new EventHandler<MouseEvent>(){
                                @Override
                                public void handle(MouseEvent event) {
                                    hoverSound.play();
                                    resultImage.setImage(new Image(new File("src/resources/tiles/ghico").toURI().toString()));
                                    hoverSound.seek(Duration.ZERO);
                                }
                            });
                            resultButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                                @Override
                                public void handle(MouseEvent event) {
                                    confirmSound.play();
                                    confirmSound.seek(Duration.ZERO);
                                    
                                    GridPane ghPane = new GridPane();
                                    ghPane.setId("ghp");
                                    ghPane.setAlignment(Pos.CENTER);
                                    
                                    MediaPlayer ghmus = new MediaPlayer(new Media((new File("src/resources/sounds/ghmus").toURI().toString())));
                                    ghmus.play();
                                    ghmus.setOnEndOfMedia(new Runnable(){
                                        @Override
                                        public void run() {
                                            try {
                                                saveTrainers();
                                            } catch (IOException ex) {
                                                Logger.getLogger(PokedexInterface.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            Platform.exit();
                                    }
                                    });
                                    
                                    ImageView ghIm = new ImageView(new Image(new File("src/resources/tiles/ghim").toURI().toString()));
                                    
                                    Circle circle = new Circle(com.sun.glass.ui.Screen.getMainScreen().getVisibleWidth()/7, com.sun.glass.ui.Screen.getMainScreen().getVisibleHeight()/4, 50);
                                    circle.setRadius(50);
                                    circle.setFill(Color.RED);
                                    circle.setStroke(Color.TRANSPARENT);
                                    
                                    PathTransition circularAnimation = new PathTransition();
                                    circularAnimation.setDuration(Duration.seconds(4));
                                    circularAnimation.setNode(ghIm);
                                    circularAnimation.setPath(circle);
                                    circularAnimation.setCycleCount(Timeline.INDEFINITE);
                                    circularAnimation.setAutoReverse(false);
                                    circularAnimation.setInterpolator(Interpolator.LINEAR);
                                    
                                    ghPane.add(ghIm, 0, 0);
                                    circularAnimation.play();
                                    
                                    Scene ghScene = new Scene(ghPane);
                                    ghScene.getStylesheets().add(getClass().getResource("searchStyle.css").toExternalForm());
                                    pokeWindow.setScene(ghScene);
                                    pokeWindow.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
                                    pokeWindow.setFullScreen(true);
                                }
                            });

                            searchResults.add(resultButton);
                        }
                        
                        else{
                            for(int c = 0; c < pokemonHollowList.size(); c++){
                                String inputCapitalized = inputLowercase.substring(0, 1).toUpperCase() + inputLowercase.substring(1);
                                final int counterC = c;
                                if((pokemonHollowList.get(c).getName().contains(inputLowercase))||(pokemonHollowList.get(c).getName().contains(inputCapitalized))){
                                    if(c < 802){
                                    pokemonHollowList.get(c).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (c + 1) + ".png"));
                                }

                                if((c > 802)&&(c < 892)){
                                    pokemonHollowList.get(c).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (c + 9199) + ".png"));
                                }

                                if(c > 892){
                                    pokemonHollowList.get(c).setPokeImage(new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString()));
                                }


                                BufferedImage croppedImage = cropImage(SwingFXUtils.fromFXImage(pokemonHollowList.get(c).getPokeImage(), null));
                                Image image = SwingFXUtils.toFXImage(croppedImage, null);

                                //PixelReader resizing sample from jewelsea on Github gist
                                    int imWidth = (int) croppedImage.getWidth();
                                    int imHeight = (int) croppedImage.getHeight();

                                    WritableImage output = new WritableImage(imWidth * 2, imHeight * 2);

                                    PixelReader reader = image.getPixelReader();
                                    PixelWriter writer = output.getPixelWriter();

                                    for (int y = 0; y < imHeight; y++) {
                                        for (int x = 0; x < imWidth; x++) {
                                            final int argb = reader.getArgb(x, y);
                                            for (int dy = 0; dy < 2; dy++) {
                                                for (int dx = 0; dx < 2; dx++) {
                                                    writer.setArgb(x * 2 + dx, y * 2 + dy, argb);
                                                }
                                            }
                                        }
                                    }
                                //PixelReader resizing sample from jewelsea on Github gist

                                    Button resultButton = new Button(pokemonHollowList.get(c).getName().substring(0, 1).toUpperCase() + pokemonHollowList.get(c).getName().substring(1));
                                    resultButton.setId("resultbutton");
                                    resultButton.setMinSize(199, 42);
                                    resultButton.setPrefSize(199, 42);
                                    resultButton.setMaxSize(199, 42);
                                    resultButton.setOnMouseEntered(new EventHandler<MouseEvent>(){
                                        @Override
                                        public void handle(MouseEvent event) {
                                            hoverSound.play();
                                            resultImage.setImage(output);
                                            hoverSound.seek(Duration.ZERO);
                                        }
                                    });
                                    resultButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                                        @Override
                                        public void handle(MouseEvent event) {
                                            confirmSound.play();
                                            globalId = pokemonHollowList.get(counterC).getId();
                                            globalPos = counterC;
                                            targetScene = "summaryScene";
                                            targettedPokemon = -1;
                                            confirmSound.seek(Duration.ZERO);
                                            loadScene();
                                        }
                                    });

                                    searchResults.add(resultButton);
                                }
                            }
                        }
                    }
                    
                    if(typeOption.isSelected()){
                        String inputLowercase = searchInput.getCharacters().toString().toLowerCase();
                        int target = 42;
                        if(inputLowercase.equals("normal")){
                            target = 1;
                        }
                        
                        if((inputLowercase.equals("fighting"))||(inputLowercase.equals("fight"))){
                            target = 2;
                        }
                        
                        if((inputLowercase.equals("flying"))||(inputLowercase.equals("fly"))){
                            target = 3;
                        }
                        
                        if(inputLowercase.equals("poison")){
                            target = 4;
                        }
                        
                        if(inputLowercase.equals("ground")){
                            target = 5;
                        }
                        
                        if(inputLowercase.equals("rock")){
                            target = 6;
                        }
                        
                        if(inputLowercase.equals("bug")){
                            target = 7;
                        }
                        
                        if(inputLowercase.equals("ghost")){
                            target = 8;
                        }
                        
                        if(inputLowercase.equals("steel")){
                            target = 9;
                        }
                        
                        if(inputLowercase.equals("fire")){
                            target = 10;
                        }
                        
                        if(inputLowercase.equals("water")){
                            target = 11;
                        }
                        
                        if(inputLowercase.equals("grass")){
                            target = 12;
                        }
                        
                        if(inputLowercase.equals("electric")){
                            target = 13;
                        }
                        
                        if(inputLowercase.equals("psychic")){
                            target = 14;
                        }
                        
                        if(inputLowercase.equals("ice")){
                            target = 15;
                        }
                        
                        if(inputLowercase.equals("dragon")){
                            target = 16;
                        }
                        
                        if(inputLowercase.equals("dark")){
                            target = 17;
                        }
                        
                        if(inputLowercase.equals("fairy")){
                            target = 18;
                        }
                        
                        if(inputLowercase.equals("shadow")){
                            target = 20;
                        }
                        
                        if(target != 42){
                            boolean alreadyCaught = false;
                            int c;
                            for(c = 0; c < caughtTypes.size(); c++){
                                if(target == caughtTypes.get(c).intValue()){
                                    alreadyCaught = true;
                                    break;
                                }
                            }

                            JSONArray resultsArray = null;
                            if(alreadyCaught){
                                resultsArray = typesArray.get(c);
                            }
                            else{
                                JSONCapturer typeCapturer = null;
                                try {
                                    typeCapturer = new JSONCapturer(new URL("https://pokeapi.co/api/v2/"));
                                } catch (MalformedURLException ex) {
                                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                try {
                                    resultsArray = typeCapturer.returnTypeArray(target);
                                } catch (IOException ex) {
                                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (ParseException ex) {
                                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }

                            for(int x = 0; x < resultsArray.size(); x++){
                                    JSONObject pokemonObj = (JSONObject) resultsArray.get(x);
                                    pokemonObj = (JSONObject) pokemonObj.get("pokemon");
                                    String pokemon = pokemonObj.get("name").toString();

                                    int z;
                                    for(z = 0; z < pokemonHollowList.size(); z++){
                                        if(pokemonHollowList.get(z).getName().equals(pokemon)){
                                            break;
                                        }
                                    }

                                    String inputCapitalized = pokemonHollowList.get(z).getName().substring(0, 1).toUpperCase() + pokemonHollowList.get(z).getName().substring(1);
                                    final int counterZ = z;

                                    if(z < 802){
                                        pokemonHollowList.get(z).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (z + 1) + ".png"));
                                    }

                                    if((z > 802)&&(z < 892)){
                                        pokemonHollowList.get(z).setPokeImage(new Image("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + (z + 9199) + ".png"));
                                    }

                                    if(z > 892){
                                        pokemonHollowList.get(z).setPokeImage(new Image(new File("src/resources/tiles/noPokeImage.png").toURI().toString()));
                                    }

                                    BufferedImage croppedImage = cropImage(SwingFXUtils.fromFXImage(pokemonHollowList.get(z).getPokeImage(), null));
                                    Image image = SwingFXUtils.toFXImage(croppedImage, null);

                                //PixelReader resizing sample from jewelsea on Github gist
                                    int imWidth = (int) croppedImage.getWidth();
                                    int imHeight = (int) croppedImage.getHeight();

                                    WritableImage output = new WritableImage(imWidth * 2, imHeight * 2);

                                    PixelReader reader = image.getPixelReader();
                                    PixelWriter writer = output.getPixelWriter();

                                    for (int y = 0; y < imHeight; y++) {
                                        for (int j = 0; j < imWidth; j++) {
                                            final int argb = reader.getArgb(j, y);
                                            for (int dy = 0; dy < 2; dy++) {
                                                for (int dx = 0; dx < 2; dx++) {
                                                    writer.setArgb(j * 2 + dx, y * 2 + dy, argb);
                                                }
                                            }
                                        }
                                    }
                                //PixelReader resizing sample from jewelsea on Github gist

                                    Button resultButton = new Button(inputCapitalized);
                                    resultButton.setId("resultbutton");
                                    resultButton.setMinSize(199, 42);
                                    resultButton.setPrefSize(199, 42);
                                    resultButton.setMaxSize(199, 42);
                                    resultButton.setOnMouseEntered(new EventHandler<MouseEvent>(){
                                        @Override
                                        public void handle(MouseEvent event) {
                                            hoverSound.play();
                                            resultImage.setImage(output);
                                            hoverSound.seek(Duration.ZERO);
                                        }
                                    });
                                    resultButton.setOnMouseClicked(new EventHandler<MouseEvent>(){
                                        @Override
                                        public void handle(MouseEvent event) {
                                            confirmSound.play();
                                            globalId = pokemonHollowList.get(counterZ).getId();
                                            globalPos = counterZ;
                                            targetScene = "summaryScene";
                                            targettedPokemon = -1;
                                            confirmSound.seek(Duration.ZERO);
                                            loadScene();
                                        }
                                    });

                                    searchResults.add(resultButton);
                                }
                            
                            if(!alreadyCaught){
                                typesArray.add(resultsArray);
                                caughtTypes.add(target);
                            }
                        }
                    }
                    
                    VBox resultsBox = new VBox();
                    resultsBox.setAlignment(Pos.CENTER);
                    resultsBox.getChildren().addAll(searchResults);
                    resultsBox.setTranslateX(15);
                    
                    resultScroll.setContent(resultsBox);
                }
            }
        });
        
        rightGrid.add(searchObjects, 0, 0);
        rightGrid.add(resultScroll, 0, 1);
        leftGrid.add(resultImage, 0, 0);
        
        middleGrid.add(leftGrid, 0, 0);
        middleGrid.add(rightGrid, 1, 0);
        root.add(middleGrid, 0, 1);
        
        Button back = new Button();
        back.setMinWidth(28);
        back.setPrefWidth(28);
        back.setMinHeight(28);
        back.setPrefHeight(28);
        back.setId("back");
        back.setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                backSound.play();
                backSound.seek(Duration.ZERO);
                pokeWindow.setScene(mainScene);
                
            }
        });
        
        GridPane backPane = new GridPane();
        backPane.setAlignment(Pos.CENTER_RIGHT);
        
        backPane.add(back, 0, 0);
        
        back.setTranslateX(-20);
        
        root.add(backPane, 0, 2);
        
        Scene searchScene = new Scene(root);
        searchScene.getStylesheets().add(getClass().getResource("searchStyle.css").toExternalForm());
        
        this.pokeWindow.setScene(searchScene);
    }
    
    @Override
    public void start(Stage unusedWindow) throws Exception {
        this.pokeWindow.initStyle(StageStyle.UNDECORATED);
        this.pokeWindow.setTitle("Pokedéx");
        this.pokeWindow.setWidth(512);
        this.pokeWindow.setHeight(416);
        this.pokeWindow.setResizable(false);
        this.pokeWindow.setOnCloseRequest(new EventHandler<WindowEvent>(){
            @Override
            public void handle(WindowEvent event) {
                try {
                    saveTrainers();
                } catch (IOException ex) {
                    Logger.getLogger(pokedexAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                Platform.exit();
            }
        });
        
        setMainScene();
    }
    
}