package pokedex;

import javafx.scene.image.Image;

public class HollowPokemon {
    
        private String name;
        private int id;
        private Image pokeImage;
        
        public HollowPokemon(String name, int id, Image pokeImage){
            setName(name);
            setId(id);
            setPokeImage(pokeImage);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            if(id < 802){
                this.id = id + 1;
            }
            else{
                this.id = id + 9199;
            }
        }

        public Image getPokeImage() {
            return pokeImage;
        }

        public void setPokeImage(Image pokeImage) {
            this.pokeImage = pokeImage;
        }
            
}
